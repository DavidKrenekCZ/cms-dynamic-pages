## Settings
- All settings are taken from file `./config/asgard/dynamicpages/core.php` (see `./Modules/Dynamicpages/Config/core.example` for format)

## Installation
- You need to add a few things to arrays in `config/app.php`
``` php
	'aliases' => [
		// ...
		'Image' => Intervention\Image\Facades\Image::class,
		// ...
	],

	'providers' => [
		// ...
		Intervention\Image\ImageServiceProvider::class,
		Spatie\Sitemap\SitemapServiceProvider::class,
		// ...
	]
```

#### FileUplaoder
- DynamicPages use FileUploader plugin (https://innostudio.de/fileuploader/) - because this plugin is paid, you need to install it by yourself
- Either copy your files to `Modules\Dynamicpages\Assets\fileuploader` folder, or use installation command (repository with plugin is needed)
- Run command `php artisan dynamicpages:install:fileuploader` and enter plugin repo URL in desired format (`https://{name}:{password}@{server}/{repo}`)
	- **Warning:** You need to encode special characters in your username/password to percent encoding:
		- `!` = `%21`, `#` = `%23`, `$` = `%24`, `&` = `%26`, `'` = `%27`, `(` = `%28`, `)` = `%29`, `*` = `%2A`, `+` = `%2B`   
		- `,` = `%2C`, `/` = `%2F`, `:` = `%3A`, `;` = `%3B`, `=` = `%3D`, `?` = `%3F`, `@` = `%40`, `[` = `%5B`, `]` = `%5D`
		- For example, if your password is `pass+`, write it like `pass%2B`
- You can also chose a version to checkout (latest version is used by default)
- The plugin is automatically cloned and installed after you run the command. You can also run it again in order to pull the repository.