<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCategoriesTable extends Migration {
    public function up() {
        DB::statement("ALTER TABLE `dynamicpages__categories` MODIFY COLUMN `content_type` ENUM('record', 'list', 'subcategory', 'url', 'empty', 'homepage', 'link')");
    }

    public function down() {
        DB::statement("ALTER TABLE `dynamicpages__categories` MODIFY COLUMN `content_type` ENUM('record', 'list', 'url', 'empty')");
    }
}
