<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__templates', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
        
            $table->boolean("settings_files")->default(0);
            $table->boolean("settings_images")->default(0);
            $table->boolean("settings_url")->default(0);
            $table->boolean("settings_meta")->default(0);
            $table->boolean("category")->default(0);
            $table->boolean("main_image")->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamicpages__templates');
    }
}
