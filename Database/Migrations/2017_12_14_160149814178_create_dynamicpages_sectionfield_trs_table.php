<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesSectionFieldTrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__sectionfield_trs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('section_field_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['section_field_id', 'locale']);
            $table->foreign('section_field_id')->references('id')->on('dynamicpages__sectionfields')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__sectionfield_trs', function (Blueprint $table) {
            $table->dropForeign(['section_field_id']);
        });
        Schema::dropIfExists('dynamicpages__sectionfield_trs');
    }
}
