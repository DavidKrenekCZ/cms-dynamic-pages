<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesRecordValueTrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__recordvalue_trs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string("value", 25000);

            $table->integer('record_value_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['record_value_id', 'locale']);
            $table->foreign('record_value_id')->references('id')->on('dynamicpages__recordvalues')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__recordvalue_trs', function (Blueprint $table) {
            $table->dropForeign(['record_value_id']);
        });
        Schema::dropIfExists('dynamicpages__recordvalue_trs');
    }
}
