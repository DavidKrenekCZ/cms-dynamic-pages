<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTitleCols extends Migration
{
    public function up()
    {
        Schema::table('dynamicpages__record_translations', function (Blueprint $table) {
            $table->string("files_title");
            $table->string("images_title");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__record_translations', function (Blueprint $table) {
            $table->dropColumn("files_title");
            $table->dropColumn("images_title");
        });
    }
}
