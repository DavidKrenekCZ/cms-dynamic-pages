<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesRecordTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__record_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string("name");
            $table->string("url");
            $table->string("meta_description");

            $table->integer('record_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['record_id', 'locale']);
            $table->foreign('record_id')->references('id')->on('dynamicpages__records')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__record_translations', function (Blueprint $table) {
            $table->dropForeign(['record_id']);
        });
        Schema::dropIfExists('dynamicpages__record_translations');
    }
}
