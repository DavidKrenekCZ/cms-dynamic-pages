<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__urls', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
        
            $table->integer("category_id")->nullable()->default(null);
            $table->integer("record_id")->nullable()->default(null);
            $table->boolean("canonical")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamicpages__urls');
    }
}
