<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesTemplateFieldTrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__templatefield_trs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string("name");

            $table->integer('template_field_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['template_field_id', 'locale']);
            $table->foreign('template_field_id')->references('id')->on('dynamicpages__templatefields')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__templatefield_trs', function (Blueprint $table) {
            $table->dropForeign(['template_field_id']);
        });
        Schema::dropIfExists('dynamicpages__templatefield_trs');
    }
}
