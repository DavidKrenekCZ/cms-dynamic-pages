<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryDescriptionFieldFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dynamicpages__record_translations', function (Blueprint $table) {
            $table->dropColumn("description");
        });
        Schema::table('dynamicpages__category_translations', function (Blueprint $table) {
            $table->string("description", 2500)->after("url")->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__record_translations', function (Blueprint $table) {
            $table->string("description", 2500)->after("url")->default("");
        });
        Schema::table('dynamicpages__category_translations', function (Blueprint $table) {
            $table->dropColumn("description");
        });
    }
}
