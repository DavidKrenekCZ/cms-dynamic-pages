<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDynamicpagesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dynamicpages__categories', function (Blueprint $table) {
            $table->enum("content_type", ["record", "list", "url"]);
            $table->integer("content_id")->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__categories', function (Blueprint $table) {
            $table->dropColumn("content_type");
            $table->dropColumn("content_id");
        });
    }
}
