<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPagesUrlTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamicpages__url_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
          
            $table->string("url", 1000);

            $table->integer('url_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['url_id', 'locale']);
            $table->foreign('url_id')->references('id')->on('dynamicpages__urls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamicpages__url_translations', function (Blueprint $table) {
            $table->dropForeign(['url_id']);
        });
        Schema::dropIfExists('dynamicpages__url_translations');
    }
}
