<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContentTypeValues extends Migration
{
    public function up()
    {
        \DB::statement("ALTER TABLE dynamicpages__categories MODIFY COLUMN content_type ENUM('record', 'list', 'url', 'empty')");
    }
    
    public function down()
    {
        \DB::statement("ALTER TABLE dynamicpages__categories MODIFY COLUMN content_type ENUM('record', 'list', 'url')");
    }
}
