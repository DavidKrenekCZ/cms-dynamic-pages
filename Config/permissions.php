<?php

return [
    'dynamicpages.templates' => [
        'index' => 'dynamicpages::templates.list resource',
        'create' => 'dynamicpages::templates.create resource',
        'edit' => 'dynamicpages::templates.edit resource',
        'destroy' => 'dynamicpages::templates.destroy resource',
    ],
    'dynamicpages.records' => [
        'index' => 'dynamicpages::records.list resource',
        'create' => 'dynamicpages::records.create resource',
        'edit' => 'dynamicpages::records.edit resource',
        'destroy' => 'dynamicpages::records.destroy resource',
    ],
    'dynamicpages.categories' => [
        'index' => 'dynamicpages::categories.list resource',
        'create' => 'dynamicpages::categories.create resource',
        'edit' => 'dynamicpages::categories.edit resource',
        'destroy' => 'dynamicpages::categories.destroy resource',
    ],
];
