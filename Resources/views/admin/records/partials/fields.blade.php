<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("name", trans('dynamicpages::records.fields.name'), $errors, $lang, $record, ["data-slug" => "source"]) !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.url") ? ' has-error' : '' }}' data-hidable="settings_url">
        {!! Form::i18nInput("url", trans('dynamicpages::records.fields.url'), $errors, $lang, $record, ["data-slug" => "target"]) !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.meta_title") ? ' has-error' : '' }}' data-hidable="settings_meta">
        {!! Form::i18nInput("meta_title", trans('dynamicpages::records.fields.meta_title'), $errors, $lang, $record) !!}
    </div>    
    <div class='form-group{{ $errors->has("{$lang}.meta_description") ? ' has-error' : '' }}' data-hidable="settings_meta">
        {!! Form::i18nInput("meta_description", trans('dynamicpages::records.fields.meta_description'), $errors, $lang, $record) !!}
    </div>

    @foreach(\Modules\DynamicPages\Entities\Template::all() as $template)
    <div data-template-only="{{ $template->id }}">
    	@foreach($template->fields()->ordered() as $field)
    		@if ($field->type == "date")
    			{!! Form::i18nInput($field->formName, "📆 ".$field->name, $errors, $lang, null, ["class" => "datepicker form-control",]) !!}
    		@elseif ($field->type == "textarea")
    			{!! Form::i18nTextarea($field->formName, $field->name, $errors, $lang, null, ["class" => "form-textarea"]) !!}
    		@elseif ($field->type == "wysiwyg")
    			{!! Form::i18nTextarea($field->formName, $field->name, $errors, $lang, null) !!}
            @elseif ($field->type == "checkbox")
                {!! Form::i18nCheckbox($field->formName, $field->name, $errors, $lang, null) !!}
            @elseif ($field->type == "tags")
                {!! Form::i18nInput($field->formName, $field->name, $errors, $lang, null, ["placeholder" => "", "data-tags-autocomplete" => "none"]) !!}
            @elseif ($field->type == "ac-tags")
                {!! Form::i18nInput($field->formName, $field->name, $errors, $lang, null, ["placeholder" => "", "data-tags-autocomplete" => "all-records"]) !!}
    		@else
    			{!! Form::i18nInput($field->formName, $field->name, $errors, $lang, null) !!}
    		@endif
    	@endforeach
    </div>
    @endforeach

    <div class='form-group{{ $errors->has("{$lang}.files_title") ? ' has-error' : '' }}' data-hidable="settings_files">
        {!! Form::i18nInput("files_title", trans('dynamicpages::records.fields.files_title'), $errors, $lang, $record) !!}
    </div>

    <div class='form-group{{ $errors->has("{$lang}.images_title") ? ' has-error' : '' }}' data-hidable="settings_images">
        {!! Form::i18nInput("images_title", trans('dynamicpages::records.fields.images_title'), $errors, $lang, $record) !!}
    </div>
</div>

@push("css-stack")
    <!-- Datepicker -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" integrity="sha256-WgFzD1SACMRatATw58Fxd2xjHxwTdOqB48W5h+ZGLHA=" crossorigin="anonymous" />

    <!-- Bootstrap tags -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha256-uKEg9s9/RiqVVOIWQ8vq0IIqdJTdnxDMok9XhiqnApU=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha256-kEEuwzcrMTLlrazS39JQIwiHUCxx2vRJlZcJ0Tj6MNU=" crossorigin="anonymous" />

	<style type="text/css">
		textarea.form-textarea {
			display: block;
			height: 10rem;
			width: 100%;
			resize: vertical;
		}

		.form-control.datepicker {
			border-radius: 0;
		}

        [data-template-only] .checkbox label {
            font-weight: bold;
            margin: 1rem 0 2rem;
        }

        [data-template-only] .checkbox label .icheckbox_flat-blue {
            float: right;
            margin-left: 1rem;
        }

        .bootstrap-tagsinput {
            display: block !important;
            padding: 6px 12px !important;
            border-radius: 0;
        }

        .bootstrap-tagsinput>input {
            
            font-size: 14px;
            color: #555;
        }
	</style>
@endpush

@push("js-stack")
    <!-- Datepicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" integrity="sha256-TueWqYu0G+lYIimeIcMI8x1m14QH/DQVt4s9m/uuhPw=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.cs.min.js" integrity="sha256-x5slhErTH7oK26gg11X6oD/V5Wu1rp5XD6Z2BlKkPZE=" crossorigin="anonymous"></script>

    <!-- Bootstrap tags -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha256-tQ3x4V2JW+L0ew/P3v2xzL46XDjEWUExFkCDY0Rflqc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js" integrity="sha256-RWiU4omUU7tQ2M3wmRQNW9UL50MB4CucbRPCbsQv+X0=" crossorigin="anonymous"></script>


	<script type="text/javascript">
		$(function() {
            // Datepicker
			$('.datepicker').datepicker({
				format: "dd. mm. yyyy",
                language: "{{ locale() }}"
			});

            // Fill inputs
            @if($record)
            var fields = {
                @foreach($record->fieldsFormValues as $name => $value) "{{ $name }}": {!! json_encode($value) !!}, @endforeach
            };

            for (var name in fields) {
                var el = $("[name='"+name+"']");
                
                if (el.attr("type") != "checkbox")
                    el.val(fields[name]);
                else
                    el.iCheck(fields[name] != "0" ? "check" : "uncheck");
            }
            @endif

            window.typeaheadRecords = {};

            $("[data-tags-autocomplete]").each(function() {
                var autocompleteType = $(this).data("tags-autocomplete");
                var autocomplete;
                if (autocompleteType == "none") {
                    autocomplete = null;
                } else if (autocompleteType == "all-records") {
                    if (!window.typeaheadRecords.all) {
                        window.typeaheadRecords.all = new Bloodhound({
                            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                            queryTokenizer: Bloodhound.tokenizers.whitespace,
                            prefetch: {
                                url: '{{ route('admin.dynamicpages.record.typeahead', ["categoryId" => "all"])."?time=".floor(time()/60) }}', // update every 60 seconds
                            }
                        });
                        window.typeaheadRecords.all.initialize();
                    }

                    autocomplete = {
                        displayKey: 'name',
                        valueKey: 'id',
                        source: window.typeaheadRecords.all.ttAdapter()
                    };
                }

                $(this).tagsinput({
                    typeaheadjs: autocomplete
                });
            });

		});
	</script>
@endpush