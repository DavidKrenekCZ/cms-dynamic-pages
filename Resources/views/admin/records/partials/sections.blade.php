<div class="box-body">
	{!! Form::normalInput("published_since", trans("dynamicpages::records.fields.published since"), $errors, (object)["published_since" => $record ? $record->published_since->format("d. m. Y H:i") : \Carbon\Carbon::now()->format("d. m. Y H:i")], ["class" => "datetimepicker form-control",]) !!}

	<div class="form-group{{ $errors->has("active_to") ? ' has-error' : '' }}">
		<label>{{ trans("dynamicpages::records.fields.published to") }}</label>
        <div class='input-group date datetimepicker'>
        	<input type='text' class="form-control" value="{{ $record && $record->published_to ? $record->published_to->format("d. m. Y H:i") : ""}}" name="published_to"/>
            <span class="input-group-addon">
            	<span class="fa fa-calendar"></span>
       		</span>
           	<span class="input-group-addon">
           		<input type="checkbox" class="flat-blue" name="no_published_to" {{ old("no_published_to") || ($record && !$record->published_to) ? "checked" : "" }}>&nbsp;&nbsp;{{ trans("dynamicpages::records.fields.no_published_to") }}
            </span>
        </div>
    </div>

	<div data-hidable="main_image" style="margin: 0 0 2rem">
		{!! Form::label('main_image', trans("dynamicpages::templates.form.main_image")) !!}
		@if (config("asgard.dynamicpages.core.main_image_info"))
			<i>{{ config("asgard.dynamicpages.core.main_image_info") }}</i><br>
		@endif
		{!! Form::file("main_image") !!}
		@if ($record && $record->mainImage)
			<a href="{{ $record->mainImage }}" target="_blank"><img src="{{ $record->mainImage }}" class="mainImageThumbnail"></a>
			<i class="fa fa-trash remove-main-image" title="Odstranit hlavn� obr�zek"></i>
			<input type="hidden" name="remove_main_image" id="remove-main-image" value="0">
		@endif
	</div>
    <div data-hidable="settings_files">
    	{!! Form::label('null', trans("dynamicpages::sections.form.settings_files")) !!}
    	<input type="file" name="files" data-fileuploader-files='{{ $record ? $record->appendedFiles : "" }}'>
    </div>
    <div data-hidable="settings_images">
    	{!! Form::label('null', trans("dynamicpages::sections.form.settings_images")) !!}
    	<input type="file" name="images" data-fileuploader-files='{{ $record ? $record->appendedImages : "" }}'>
    </div>
</div>
@foreach(\Modules\DynamicPages\Entities\Template::all() as $template)
	@if ($template->sections->count())
	<div class="box-body" data-template-only="{{ $template->id }}">
		<hr style="margin: 0">
		{!! Form::label('null', trans("dynamicpages::records.fields.sections")) !!}
		<div>
		@foreach($template->sections as $section)
			{!! Form:: normalCheckbox("sections[$section->id]", $section->name, $errors, $record ? $record->sectionsFormValues : "") !!}
		@endforeach
		</div>
	</div>
	@endif
@endforeach

@push("js-stack")
	<script src="{{ url("/modules/dynamicpages/assets/sumoselect/sumoselect.js") }}"></script>
	<script src="{{ url("/modules/dynamicpages/assets/fileuploader/jquery.fileuploader.min.js") }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js" integrity="sha256-/As5lS2upX/fOCO/h/5wzruGngVW3xPs3N8LN4FkA5Q=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/cs.js" integrity="sha256-5e0RubO3OCaMrr1X80YLiI1pxD6oYnnkLMSBXPpVu9E=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>
	<script type="text/javascript">
		window.templatesSettings = {
			@foreach(\Modules\DynamicPages\Entities\Template::all() as $template) "{{ $template->id }}": {!! $template->settingsObject !!},
			@endforeach
		};

		var templates = {
		@foreach (Modules\DynamicPages\Entities\Template::all() as $template)
			@php $categories = Modules\DynamicPages\Entities\Category::where("content_id", $template->id)->whereIn("content_type", ["list", "subcategory"])->orderBy("position")->get(); @endphp
			"{{ $template->id }}": {
				@foreach ($categories as $category) 
				"{{ $category->id }}": {
					name: "{{ $category->name }}",
					parent: {!! $category->parent ? "'".$category->parent->name."'" : "false" !!},
					@if ($record && in_array($category->id, $record->categories)) selected: true @endif
				}, 
				@endforeach },
		@endforeach
		}

		$(window).load(checkActiveTo);
		$("input[type=checkbox][name=no_published_to]").on('ifToggled', checkActiveTo);

		$(document).ready(function() {
		    // Remove main image
		    $(".remove-main-image").click(function() {
		        $(this).prev("a").hide();
		        $(this).hide();
		        $("#remove-main-image").val("1");
			});

		    // Change template on select change
			$("select[name='template_id']").on("change", templateChanged);

			// Init datetimepicker
			$('.datetimepicker').datetimepicker({
                locale: '{{ locale() }}',
                format: "DD. MM. YYYY H:mm",
                allowInputToggle: true
            });

			$(".sumoselect").SumoSelect({
				placeholder: "{{ trans("dynamicpages::records.fields.categories") }}",
				search: true,
				searchText: "{{ trans("dynamicpages::records.form.search") }}",
			    noMatch : '{{ trans("dynamicpages::records.form.no match") }}',
			    captionFormat: '{{ trans("dynamicpages::records.form.caption format") }}',
    			captionFormatAllSelected: '{{ trans("dynamicpages::records.form.caption format all selected") }}',	
			});

			templateChanged();
 
 			// File input
			$('input[name="files"]').fileuploader({
				captions: {
				@foreach (trans("dynamicpages::fileuploader") as $index => $item)
					@if (!is_array($item)) 
						{{ $index }}: {!! in_array($index, ["button", "feedback", "feedback2"]) ? $item : "\"".$item."\"" !!},
					@else 
						{{ $index }}: {
						@foreach ($item as $childIndex => $childItem)
							{{ $childIndex }}: "{!! $childItem !!}", 
						@endforeach
					},
					@endif
				@endforeach
				},
				addMore: true,
				thumbnails: {
           	 		onItemShow: function(item) {
                		// add sorter button to the item html
                		item.html.find('.fileuploader-action-remove').before('<a class="fileuploader-action fileuploader-action-sort" title="{{ trans("dynamicpages::fileuploader.sort") }}"><i></i></a>');
            		}
        		},
				sorter: {
					scrollContainer: window,
				}				
			});

			// Images input
			$('input[name="images"]').fileuploader({
				captions: {
				@foreach (trans("dynamicpages::fileuploader") as $index => $item)
					@if (!is_array($item)) 
						{{ $index }}: {!! in_array($index, ["button", "feedback", "feedback2"]) ? $item : "\"".$item."\"" !!},
					@else 
						{{ $index }}: {
						@foreach ($item as $childIndex => $childItem)
							{{ $childIndex }}: "{!! $childItem !!}", 
						@endforeach
					},
					@endif
				@endforeach
				},
				fileMaxSize: 5,
    		    extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				changeInput: ' ',
				theme: 'thumbnails',
    		    enableApi: true,
				addMore: true,
				editor: {
					@if ($record)
					onSave: function(blob, item, listEl, parentEl, newInputEl, inputEl) {
						// Image is already saved - upload edited version
						if (!item.input) {
							var loading = item.html.find(".progress-holder");
							loading.show();
							$.ajax("{{ route("admin.dynamicpages.record.image-editor", $record->id) }}", {
								type: "POST",
								data: {
									_token: "{{ csrf_token() }}",
									content: blob,
									file: item.name
								}
							}).done(function(d) {
								if (!d.ok)
									alert("{{ trans("core::core.something went wrong") }}");
							}).fail(function() {
								alert("{{ trans("core::core.something went wrong") }}");
							}).always(function() {
								loading.hide();
							});
						}
					}
					@endif
				},
				sorter: {
					placeholder: '<li class="fileuploader-item fileuploader-sorter-placeholder"><div class="fileuploader-item-inner"></div></li>',
				},
				thumbnails: {
					box: '<div class="fileuploader-items">' +
    		                  '<ul class="fileuploader-items-list">' +
							      '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
    		                  '</ul>' +
    		              '</div>',
					item: '<li class="fileuploader-item file-has-popup">' +
						       '<div class="fileuploader-item-inner">' +
    		                       '<div class="type-holder">${extension}</div>' +
    		                       '<div class="actions-holder">' +
								   	   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
    		                       '</div>' +
    		                       '<div class="thumbnail-holder">' +
    		                           '${image}' +
    		                           '<span class="fileuploader-action-popup"></span>' +
    		                       '</div>' +
    		                       '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
    		                   	   '<div class="progress-holder"><i class="fa fa-spinner fa-spin"></i></div>' +
    		                   '</div>' +
    		              '</li>',
					item2: '<li class="fileuploader-item file-has-popup">' +
						       '<div class="fileuploader-item-inner">' +
    		                       '<div class="type-holder">${extension}</div>' +
    		                       '<div class="actions-holder">' +
								   	   '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
								   	   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
    		                       '</div>' +
    		                       '<div class="thumbnail-holder">' +
    		                           '${image}' +
    		                           '<span class="fileuploader-action-popup"></span>' +
    		                       '</div>' +
    		                       '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
    		                   	   '<div class="progress-holder"><i class="fa fa-spinner fa-spin"></i></div>' +
    		                   '</div>' +
    		               '</li>',
					startImageRenderer: true,
    		        canvasImage: false,
					_selectors: {
						list: '.fileuploader-items-list',
						item: '.fileuploader-item',
						start: '.fileuploader-action-start',
						retry: '.fileuploader-action-retry',
						remove: '.fileuploader-action-remove'
					},
					onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
						var plusInput = listEl.find('.fileuploader-thumbnails-input'),
    		                api = $.fileuploader.getInstance(inputEl.get(0));
						
    		            plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();
						
						if(item.format == 'image') {
							item.html.find('.fileuploader-item-icon').hide();
						}
		
						item.html.find('.fileuploader-action-remove').before('<a class="fileuploader-action fileuploader-action-sort" title="{{ trans("dynamicpages::fileuploader.sort") }}"><i></i></a>');
					}
				},
        		dragDrop: {
					container: '.fileuploader-thumbnails-input'
				},
				afterRender: function(listEl, parentEl, newInputEl, inputEl) {
					var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));
		
					plusInput.on('click', function() {
						api.open();
					});

					setTimeout(function() {
						listEl.find(".fileuploader-item-image img").each(function() {
							$(this).attr("src", $(this).attr("src")+"?updated="+Math.floor(Math.random() * 10000));
						});
					}, 1000);
				},
        		onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
        		    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));
				
					if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
        		        plusInput.show();
        		}
    		});
		});

		function checkActiveTo() {
            if ($("input[type=checkbox][name=no_published_to]:checked").length)
                $("input[type=checkbox][name=no_published_to]").closest(".input-group").find("input[type=text]").attr("disabled", "disabled");
            else
                $("input[type=checkbox][name=no_published_to]").closest(".input-group").find("input[type=text]").removeAttr("disabled");
        }

		function templateChanged() {
			var templateId = $("select[name='template_id']").val();
			// Template inputs
			for (var index in templatesSettings[templateId])
				$("[data-hidable='"+index+"']").toggleClass("hide", templatesSettings[templateId][index] != 1);


			var categories = {};
			for (var id in templates[templateId]) {
				var item = templates[templateId][id];

				if (!categories[item.parent])
					categories[item.parent] = [];
				categories[item.parent].push({
					id: id,
					name: item.name,
					selected: item.selected
				});
			}

			$('.sumoselect').html("");
			for (var group in categories) {
				var items = categories[group];

				if (group != "false")
					var append = "<optgroup label='"+group+"'>";
				else
					var append = "";

				for (var index in items) {
					var item = items[index];
					append += "<option value='"+item.id+"'"+(item.selected ? " selected" : "")+">"+item.name+"</option>";
				}

				if (group != "false")
					append += "</optgroup>";

				$(".sumoselect").append(append);
			}
			
			$('.sumoselect')[0].sumo.reload();

			// Template sections
			$("[data-template-only]").addClass("hide");
			$("[data-template-only='"+templateId+"']").removeClass("hide");
		}
	</script>
@endpush

@push("css-stack")
	<link rel="stylesheet" type="text/css" href="{{ url("/modules/dynamicpages/assets/sumoselect/sumoselect.css") }}">
	<link rel="stylesheet" type="text/css" href="{{ url("/modules/dynamicpages/assets/fileuploader/jquery.fileuploader.min.css") }}">
	<link rel="stylesheet" type="text/css" href="{{ url("/modules/dynamicpages/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css") }}">
	<link rel="stylesheet" type="text/css" href="{{ url("/modules/dynamicpages/assets/fileuploader/font/font-fileuploader.css") }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />
	<style type="text/css">
		div[data-template-only] .checkbox {
			display: inline-block;
			margin-right: 2rem;
		}

		.SumoSelect {
			width: 100%;
			display: block;
		}

		.SumoSelect>.CaptionCont {
			border-color: #d2d6de;
		}

		.SumoSelect .select-all.partial>span i, .SumoSelect .select-all.selected>span i, .SumoSelect>.optWrapper.multiple>.options li.opt.selected span i {
			background-color: #3c8dbc;
		}

		.SumoSelect>.optWrapper>.options li.group>label {
			padding: 10px 15px 0;
			margin: 0;
		}

		.SumoSelect .select-all>span, .SumoSelect>.optWrapper.multiple>.options li.opt span {
			margin-left: -30px;
		}

		.SumoSelect>.optWrapper.multiple>.options li ul li.opt {
			padding-left: 45px;
		}

		.SumoSelect .select-all>label, .SumoSelect>.CaptionCont, .SumoSelect>.optWrapper>.options li.opt label {
			margin-bottom: 0;
			font-weight: 500;
		}

		.mainImageThumbnail {
			width: 70px;
			height: 70px;
			display: inline-block;
			border: 1px solid black;
			border-radius: 3px;
		}

		div[data-hidable="main_image"] input {
			display: inline-block;
		}

		div[data-hidable="main_image"] label {
			display: block;
		}

		.remove-main-image {
			cursor: pointer;
		}

		.fileuploader-item .progress-holder {
			z-index: 10 !important;
			background: rgba(255,255,255,.5) !important;
		}

		.fileuploader-item .progress-holder .fa {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			font-size: 3rem;
		}
	</style>
@endpush