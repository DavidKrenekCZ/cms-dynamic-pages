<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script type="text/javascript">
	var cachedElements = {
		len: {
			el: "#DataTables_Table_0_length select:first",
			default: 25
		},
		template: {
			el: "select[name='template_id']",
			default: 0
		},
		category: {
			el: "select[name='category_id']",
			default: 0
		}
	};

	// Set values of all selects as they're in cache
	function loadFromCache() {
		var cache = Cookies.get("filter-cache");
		if (cache) {
			cache = JSON.parse(cache);

			for (var index in cache)
				$(cachedElements[index].el).trigger("change").val(cache[index]).trigger("change");
		}
	}

	// Save values of all selects to cache
	function saveToCache() {
		var values = {};
		for (var index in cachedElements)
			values[index] = $(cachedElements[index].el).val();
		
		Cookies.set("filter-cache", values);
	}

	// Clear cache and set values to defaults
	function clearCache() {
		Cookies.remove("filter-cache");

		for (var index in cachedElements) {
			forceChanged = true;
			$(cachedElements[index].el).val(cachedElements[index].default).change();
		}
	}

	// Clear from cache on button click
	$(".clear-cache-button .btn").click(clearCache);

	$(window).load(function() {
		// Load from cache at page load
       	loadFromCache();

       	// Save to cache at every select's change
       	for (var index in cachedElements)
       		$(cachedElements[index].el).on("change", function(d) {
       			saveToCache();
       		});

       	setTimeout(function() {
    		if (goToRecordId)
       			goToRecord(goToRecordId)
       		$(".records-loading").addClass("hide");
       	}, 500);
    });
</script>