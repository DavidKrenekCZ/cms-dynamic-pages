@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('dynamicpages::records.title.records') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('dynamicpages::records.title.records') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a class="btn btn-danger btn-flat multidelete-button-label" data-toggle="modal" data-target="#modal-multidelete-confirmation" style="padding: 4px 10px; margin-right: 1rem">
                        <i class="fa fa-trash"></i> {{ trans('dynamicpages::records.button.delete selected') }}
                    </a>
                    <a href="{{ route('admin.dynamicpages.record.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('dynamicpages::records.button.create record') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <div class="appendable-filter">
                            {!! Form::normalSelect('template_id', trans("dynamicpages::records.table.template filter"), $errors,  Modules\DynamicPages\Entities\Template::allArray(), null, ["class" => "form-control input-sm"]) !!}
                            {!! Form::normalSelect('category_id', trans("dynamicpages::records.table.category filter"), $errors,  Modules\DynamicPages\Entities\Category::filterArray(), null, ["class" => "form-control input-sm"]) !!}
                            <div class="clear-cache-button">
                                <a href="javascript:void(0)" class="btn btn-default btn-flat"><i title="{{ trans("dynamicpages::records.button.clear cache") }}" class="fa fa-refresh"></i></a>
                            </div>
                        </div>
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center"><i class="fa fa-trash"></i></th>
                                <th>{{ trans('dynamicpages::records.fields.published') }}</th>
                                <th>{{ trans('dynamicpages::records.fields.name') }}</th>
                                <th>{{ trans('dynamicpages::records.fields.url') }}</th>
                                <th>{{ trans("dynamicpages::templates.title.template") }}</th>
                                {{--<th>{{ trans("dynamicpages::records.table.visible") }}</th>--}}
                                {{--<th>{{ trans("dynamicpages::sections.title.sections") }}</th>--}}
                                <th>{{ trans("dynamicpages::categories.title.categories") }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($records)): ?>
                            <?php foreach ($records as $record): ?>
                            <tr data-record-id="{{ $record->id }}">
                                <td class="text-center">
                                    <input type="checkbox" name="multidelete" class="flat-blue" data-id="{{ $record->id }}">
                                </td>
                                <td data-order="{{ $record->published_since->format("Y-m-d H:i") }}">
                                    {{ $record->published_since->format("d. m. Y H:i") }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.dynamicpages.record.edit', [$record->id]) }}">
                                        {{ $record->name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.dynamicpages.record.edit', [$record->id]) }}">
                                        {{ $record->url() }}
                                    </a>
                                </td>
                                <td data-order="{{ $record->template_id }}">
                                    {{ $record->template->name or "(šablona nenalezena)" }}
                                </td>
                                {{--<td data-order="{{ $record->visible }}">
                                    @if ($record->visible)
                                        <i class="fa fa-check text-green"></i>
                                    @else
                                        <i class="fa fa-times text-red"></i>
                                    @endif
                                </td>--}}
                                {{--
                                <td>
                                    {{ implode(", ", $record->sectionsNames) }}
                                </td>
                                --}}
                                <td data-order="{{ implode(";", $record->categories) }}">
                                    {{ implode(", ", $record->categoriesNames) }}
                                </td>
                                <td style="min-width: 16rem">
                                    <div class="btn-group">
                                        @if($record->visible)
                                        <a class="btn btn-info btn-flat" href="{{ route("admin.dynamicpages.record.toggle-visibility", ["record" => $record]) }}" title="{{ trans("dynamicpages::records.table.hide") }}">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        @else
                                        <a class="btn btn-info btn-flat" href="{{ route("admin.dynamicpages.record.toggle-visibility", ["record" => $record]) }}" title="{{ trans("dynamicpages::records.table.show") }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        @endif
                                        <a href="{{ route('admin.dynamicpages.record.edit', [$record->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('admin.dynamicpages.record.clone', [$record->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-clone"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.dynamicpages.record.destroy', [$record->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="overlay records-loading">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')

    <div class="modal fade modal-danger" id="modal-multidelete-confirmation" tabindex="-1" role="dialog" aria-labelledby="delete-confirmation-title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="delete-confirmation-title">{{ trans('core::core.modal.title') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="default-message">
                        {{ trans('core::core.modal.confirmation-message') }}
                    </div>
                    <div class="custom-message"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                    <button type="button" class="btn btn-outline btn-flat multidelete-button pull-left"><i class="fa fa-trash"></i> {{ trans('core::core.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('dynamicpages::records.title.create record') }}</dd>
    </dl>
@stop

@push("css-stack")
    <style type="text/css">
        @media (min-width: 768px) {
            #DataTables_Table_0_wrapper .row:first-child .col-sm-6:first-child {
                width: 75%;
            }

            #DataTables_Table_0_wrapper .row:first-child .col-sm-6:last-child {
                width: 25%;
            }
        }

        .appendable-filter {
            display: inline-block;
            position: relative;
        }

        .appendable-filter select {
            margin-left: 5px;
            padding: 5px 3px;
            width: auto !important;
        }

        .appendable-filter label {
            margin-left: 1.5rem;
        }

        .clear-cache-button {
            position: absolute;
            z-index: 10;
            right: -50px;
            top: 0;
        }

        @media screen and (max-width: 767px) {
            .appendable-filter {
                display: block;
                margin-top: 1rem;
            }

            .appendable-filter label {
                margin-left: 0;
            }
        }    

        @media screen and (max-width: 1104px) {
            .clear-cache-button {
                position: static;
                margin: 0 1rem 1rem;
            } 
        }

        .data-table tbody tr td.text-center .icheckbox_flat-blue {
            transform: scale(.8);
        } 
    </style>
@endpush

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "@php route('admin.dynamicpages.record.create') @endphp" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').on("init.dt", function() {
                $(".appendable-filter").appendTo("#DataTables_Table_0_length");
                $(".appendable-filter select").prepend("<option value='0'>{{ trans("dynamicpages::records.table.no filter") }}</option>").val(0);

                // Template and category filter
                $(".appendable-filter select, select[name='DataTables_Table_0_length']").on("change", renderFilters);

                @if (isset($templateFilter) && $templateFilter)
                    $(".appendable-filter div:first-of-type select").val({{ $templateFilter }}).change();
                @endif
            }).dataTable({
                "paginate": true,
                "lengthChange": true,
                "pageLength": 25,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 1, "desc" ]],
                "language": {
                    "url": '{{ url("/public/modules/core/js/vendor/datatables/{$locale}.json") }}'
                }
            });
        });

        // Go to page that contains row with ID from GET parameter
        var goToRecordId = {{ $goToRecord ? $goToRecord : "false" }};
        function goToRecord(id) {
            var table = $(".table").DataTable()

            var perPage = table.context[0]._iDisplayLength
            var rows = table.$("tr", { "filter": "applied" });

            // Just one page
            if (rows.length <= perPage) return;

            var row = false

            for (var i in rows) {
                if (typeof rows[i] != "object")
                    break

                if ($(rows[i]).data("record-id") == id) {
                    row = i
                    break
                }
            }

            // Row with ID not found
            if (!row) return;

            var page = Math.floor(row / perPage)
            table.page(page).draw( 'page' )
        }

        function renderFilters() {
            $.fn.dataTableExt.afnFiltering.length = 0;
            filterByTemplate(4, $(".appendable-filter div:first-of-type select").val());
            filterByCategory(5, $(".appendable-filter div:nth-of-type(2) select").val());

            $(".data-table").dataTable().fnDraw();
        }

        function filterByTemplate(column, templateId) {
            $.fn.dataTableExt.afnFiltering.push(
                function(a, b, c , data) {
                    if (templateId == 0)
                        return true;
                    return (data[column]["@data-order"] == templateId);
            }
            );
        };

        function filterByCategory(column, categoryId) {
            $.fn.dataTableExt.afnFiltering.push(
                function(a, b, c , data) {
                    if (categoryId == 0)
                        return true;
                    return (data[column]["@data-order"].split(";").indexOf(categoryId) > -1);
            }
            );
        };

        function multideleteChanged() {
            $(".multidelete-button-label").toggleClass("disabled", !$(".icheckbox_flat-blue.checked").length);
        }

        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            multideleteChanged();
            $('input[name=multidelete]').on('ifToggled', function() {
                setTimeout(multideleteChanged, 100);
            });

            $(".multidelete-button").click(function() {
                var ids = [];
                $(".icheckbox_flat-blue.checked").each(function() {
                    ids.push($(this).find("input[type=checkbox]").data("id"));
                });

                $(".records-loading").show();
                $("#modal-multidelete-confirmation").modal("hide")
                $.ajax("{{ route("admin.dynamicpages.records.multidelete") }}", {
                    data: {
                        ids: ids,
                        _token: "{{ csrf_token() }}"
                    },
                    type: "post"
                }).done(function(d) {
                    document.location = "";
                }).fail(function() {
                    alert("Záznamy se nepodařilo odstranit, opakujte akci.");
                    $(".records-loading").hide();
                });
            });
        });
    </script>

    @include("dynamicpages::admin.records.filter-cache")
@endpush
