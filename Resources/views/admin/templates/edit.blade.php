@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('dynamicpages::templates.title.edit template') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.dynamicpages.template.index') }}">{{ trans('dynamicpages::templates.title.templates') }}</a></li>
        <li class="active">{{ trans('dynamicpages::templates.title.edit template') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.dynamicpages.template.update', $template->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" data-locale="{{ $locale }}" id="tab_{{ $i }}">
                            @include('dynamicpages::admin.templates.partials.fields', ['lang' => $locale])
                        </div>
                    @endforeach


                    <div class="box-body">
                        <table class="fields table">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        {{ trans('dynamicpages::templates.form.field position') }}
                                    </th>
                                    <th>
                                        {{ trans('dynamicpages::templates.form.field name') }}
                                    </th>
                                    <th>
                                        {{ trans('dynamicpages::templates.form.field type') }}
                                    </th>
                                    <th>
                                        {{ trans('dynamicpages::templates.form.field system name') }}
                                    </th>
                                    <th class="text-center">
                                        {{ trans('dynamicpages::templates.form.field remove') }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="copiable-fields hide field-row">
                                    <td class="text-center sortable-handle-icon">
                                        <i class="fa fa-arrows"></i>
                                    </td>
                                    <td class="field-name">
                                        @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                                            {!! Form::normalInput("field_name_".$locale."[]", trans('dynamicpages::templates.form.field name'), $errors, null, [ "data-locale" => $locale ]) !!}
                                        @endforeach
                                    </td>
                                    <td>
                                        {!! Form::normalSelect('field_type[]', trans("dynamicpages::templates.form.field type"), $errors, trans("dynamicpages::templates.form.types")) !!}
                                        {!! Form::hidden('field_id[]', 'new_field') !!}
                                    </td>
                                    <td class="field-name">
                                        {!! Form::normalInput("field_system_name[]", trans('dynamicpages::templates.form.field system name'), $errors, null) !!}
                                    </td>
                                    <td class="text-center">
                                        <a class="delete-field" onclick='$(this).closest("tr").remove();'>
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>

                                @foreach($template->fields()->ordered() as $field)
                                <tr class="field-row">
                                    <td class="text-center sortable-handle-icon">
                                        <i class="fa fa-arrows"></i>
                                    </td>
                                    <td class="field-name">
                                        @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                                            {!! Form::normalInput("field_name_".$locale."[]", trans('dynamicpages::templates.form.field name'), $errors, (object)["field_name_".$locale."[]" => $field->translate($locale)->name], [ "data-locale" => $locale ]) !!}
                                        @endforeach
                                    </td>
                                    <td>
                                        {!! Form::normalSelect('field_type[]', trans("dynamicpages::templates.form.field type"), $errors, trans("dynamicpages::templates.form.types"), (object)["field_type[]" => $field->type]) !!}
                                        {!! Form::hidden('field_id[]', $field->id) !!}
                                    </td>
                                    <td class="field-name">
                                        {!! Form::normalInput("field_system_name[]", trans('dynamicpages::templates.form.field system name'), $errors, (object)["field_system_name[]" => $field->system_name], [ "placeholder" => mb_strlen($field->system_name) > 0 ? $field->system_name : $field->name  ]) !!}
                                    </td>
                                    <td class="text-center">
                                        <a class="delete-field" onclick='$(this).closest("tr").remove();'>
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr> 
                                @endforeach                              
                            </tbody>
                        </table>
                        <div class="text-center">
                            <button type="button" class="btn btn-primary btn-flat add-another-field">{{ trans('dynamicpages::templates.button.add field') }}</button>
                        </div>
                    </div>

                    <hr>
                    <div class="box-body">
                        {!! Form:: normalCheckbox('settings_files', trans("dynamicpages::templates.form.settings_files"), $errors, $template) !!}
                        {!! Form:: normalCheckbox('settings_images', trans("dynamicpages::templates.form.settings_images"), $errors, $template) !!}
                        {!! Form:: normalCheckbox('settings_url', trans("dynamicpages::templates.form.settings_url"), $errors, $template) !!}
                        {!! Form:: normalCheckbox('settings_meta', trans("dynamicpages::templates.form.settings_meta"), $errors, $template) !!}
                        {!! Form:: normalCheckbox('category', trans("dynamicpages::templates.form.category"), $errors, $template) !!}
                        {!! Form:: normalCheckbox('main_image', trans("dynamicpages::templates.form.main_image"), $errors, $template) !!}
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.dynamicpages.template.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push("css-stack")
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
    <style type="text/css">
        table.fields td.field-name .form-group {
            margin: 0;
        }

        table.fields td .form-group label {
            display: none;
        }

        table.fields td .delete-field {
            cursor: pointer;
        }

        table.fields tbody tr.field-row td {
            background-color: white;
        }

        table.fields tbody tr.field-row td.sortable-handle-icon {
            cursor: move;
        }

        table.fields tbody td div.form-group.dropdown {
            margin-bottom: 0;
        }
    </style>
@endpush

@push('js-stack')
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "@php route('admin.dynamicpages.template.index') @endphp" }
                ]
            });

            updateFields();
        });

        $(".nav-tabs-custom ul.nav.nav-tabs li").click(function() {
            setTimeout(updateFields, 100);
        });

        $(".add-another-field").click(appendField);

        function updateFields() {
            $(".tab-content .tab-pane").each(function() {
                var inputs = $("table.fields td.field-name .form-group input[data-locale='"+$(this).data("locale")+"']");
                if ($(this).hasClass("active"))
                    inputs.removeClass("hide");
                else
                    inputs.addClass("hide");

            });
        }

        function appendField() {
            $("table.fields tbody tr.copiable-fields").clone().removeClass("copiable-fields hide").appendTo($("table.fields tbody"));
        }

        $("table.fields tbody").sortable({
                placeholder: "ui-state-highlight",
                handle: ".sortable-handle-icon"
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
