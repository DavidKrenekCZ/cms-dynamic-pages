<div class="modal fade modal-default" id="add-section-modal" tabindex="-1" role="dialog" aria-labelledby="add-section-title" aria-hidden="true">
    {!! Form::open(['route' => ['admin.dynamicpages.section.store'], 'method' => 'post']) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="add-section-title">
                    <span class="create-title">{{ trans("dynamicpages::sections.title.create section") }}</span>
                    <span class="edit-title">{{ trans("dynamicpages::sections.title.edit section") }}</span>
                </h4>
            </div>
            <div class="modal-body">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            <div class="box-body">
                                {!! Form::i18nInput("name", trans('dynamicpages::templates.form.name'), $errors, $locale) !!}
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="box-body">
                    <hr style="margin: 0 0 5px 0">
                    <table class="table no-border">
                    @foreach($templates as $template)
                        @foreach($template->fields()->ordered() as $field)
                            <tr class="hide fields-row" data-id="{{ $template->id }}" data-field-id="{{ $field->id }}">
                                <td>{{ $field->name }}</td>
                                <td>{!! Form:: normalCheckbox('sectionfields_'.$template->id."[$field->id]", trans("dynamicpages::sections.table.show field"), $errors, null) !!}</td>
                            </tr>
                        @endforeach
                    @endforeach
                        <tr id="settings_files_row">
                            <td>{{ trans("dynamicpages::sections.form.settings_files") }}</td>
                            <td>{!! Form:: normalCheckbox('settings_files', trans("dynamicpages::sections.table.show field"), $errors, null) !!}</td>
                        </tr>
                        <tr id="settings_images_row">
                            <td>{{ trans("dynamicpages::sections.form.settings_images") }}</td>
                            <td>{!! Form:: normalCheckbox('settings_images', trans("dynamicpages::sections.table.show field"), $errors, null) !!}</td>
                        </tr>
                    </table>
                    <input type="hidden" name="template_id" value="0">
                    <input type="hidden" name="section_edit_id" value="0">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-left">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" data-dismiss="modal"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
            </div>
        </div>    
    </div>
    {!! Form::close() !!}
</div>

@push("css-stack")
    <style type="text/css">
        #add-section-modal .box-body .checkbox {
            margin: 0;
        }

        #add-section-modal .form-group {
            margin-bottom: 0;
        }

        #add-section-modal table.no-border {
            margin-bottom: 0;
        }
    </style>
@endpush

@push("js-stack")
    <script type="text/javascript">
        window.denyFiles = [@foreach($templates->where("settings_files", 0) as $t) {{ $t->id }}, @endforeach];
        window.denyImages = [@foreach($templates->where("settings_images", 0) as $t) {{ $t->id }}, @endforeach];

        window.createRoute = "{{ route("admin.dynamicpages.section.store") }}";
        window.editRoute = "{{ route("admin.dynamicpages.section.update") }}";

        window.sectionsValues = {
            @foreach(Modules\DynamicPages\Entities\Section::all() as $section)
            "{{ $section->id }}": {!! $section->jsEditObject !!},
            @endforeach
        };

        $(document).ready(function() {
            $("#add-section-modal").on('show.bs.modal', function (e) {
                var btn = $(e.relatedTarget);
                var form = $("#add-section-modal form");
                var templateId = btn.data("id");
                $("#add-section-modal .fields-row").addClass("hide");
                $("#add-section-modal .fields-row[data-id='"+templateId+"']").removeClass("hide");
                $("#add-section-modal input[type='hidden'][name='template_id']").val(templateId);

                $("#settings_files_row").toggleClass("hide", denyFiles.indexOf(templateId) != -1);
                $("#settings_images_row").toggleClass("hide", denyImages.indexOf(templateId) != -1);

                $("#add-section-title .create-title").addClass("hide");
                $("#add-section-title .edit-title").removeClass("hide");

                if (btn.data("type") == "edit") {
                    var sectionId = btn.data("section-id");
                    $("#add-section-modal input[type='hidden'][name='section_edit_id']").val(sectionId);
                    form.attr("action", editRoute);

                    var values = sectionsValues[sectionId];
                    for (var index in values.name)
                        $("#add-section-modal .tab-content .form-group input[name='"+index+"']").val(values.name[index]);

                    for (var id in values.sectionsfields) {
                        var checkbox = $("#add-section-modal .fields-row[data-field-id='"+id+"'] input[type=checkbox]");
                        if (values.sectionsfields[id] == 1)
                            checkbox.iCheck("check");
                        else
                            checkbox.iCheck("uncheck");
                    }

                    for (var set in values.settings) {
                        var checkbox = $("#add-section-modal input[type=checkbox][name='settings_"+set+"']");
                        if (values.settings[set] == 1)
                            checkbox.iCheck("check");
                        else
                            checkbox.iCheck("uncheck");
                    }
                } else {
                    form.attr("action", createRoute);
                    $("#add-section-modal .tab-content .form-group input").val("");
                    $('#add-section-modal input[type=checkbox]').iCheck('uncheck');
                    $("#add-section-title .create-title").removeClass("hide");
                    $("#add-section-title .edit-title").addClass("hide");
                }
            });
        });
    </script>
@endpush