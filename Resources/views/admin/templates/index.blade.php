@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('dynamicpages::templates.title.templates') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('dynamicpages::templates.title.templates') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.dynamicpages.template.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('dynamicpages::templates.button.create template') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('dynamicpages::templates.form.name') }}</th>
                                <th>{{ trans('dynamicpages::templates.table.number of fields') }}</th>
                                {{--<th>{{ trans('dynamicpages::sections.title.sections') }}</th>--}}
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($templates)): ?>
                            <?php foreach ($templates as $template): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.dynamicpages.template.edit', [$template->id]) }}">
                                        {{ $template->name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.dynamicpages.template.edit', [$template->id]) }}">
                                        {{ $template->fields->count() }}
                                    </a>
                                </td>
                                {{--
                                <td>
                                    @foreach($template->sections as $section)
                                        <div class="text-center">
                                            <a style="cursor: pointer" data-id="{{ $template->id }}" data-section-id="{{ $section->id }}" data-type="edit" data-toggle="modal" data-target="#add-section-modal">
                                                {{ $section->name }}
                                            </a>
                                            <a data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.dynamicpages.section.destroy', [$section->id]) }}">
                                                &nbsp;&nbsp;
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    @endforeach
                                </td>
                                --}}
                                <td>
                                    <div class="btn-group">
                                        {{--<button class="btn btn-info btn-flat" data-id="{{ $template->id }}" data-toggle="modal" data-target="#add-section-modal" title="{{ trans("dynamicpages::sections.title.create section") }}"><i class="fa fa-plus-square"></i></button>--}}
                                        <a href="{{ route('admin.dynamicpages.template.edit', [$template->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat @if ($template->records->count()) disabled" disabled title="{{ trans('dynamicpages::templates.messages.template undeletable') }}" @else " @endif data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.dynamicpages.template.destroy', [$template->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
    @include('dynamicpages::admin.templates.sections.create', ["templates" => $templates])
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('dynamicpages::templates.title.create template') }}</dd>
    </dl>
@stop

@push("css-stack")
    <style type="text/css">
        a i.fa {
            cursor: pointer;
        }
    </style>
@endpush

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "@php route('admin.dynamicpages.category.create') @endphp" }
                ]
            });
        });
    </script>
    @php $locale = locale(); @endphp
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "pageLength": 25,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '{{ url("/public/modules/core/js/vendor/datatables/{$locale}.json") }}'
                }
            });

            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
