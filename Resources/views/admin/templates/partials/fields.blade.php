<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("name", trans('dynamicpages::templates.form.name'), $errors, $lang, $template) !!}
    </div>
</div>
