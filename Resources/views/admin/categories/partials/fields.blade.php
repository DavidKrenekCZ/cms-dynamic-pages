<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("name", trans('dynamicpages::categories.fields.name'), $errors, $lang, $category, !$category ? ["data-slug" => "source"] : []) !!}
    </div>
    <div class='hide form-group{{ $errors->has("{$lang}.url") ? ' has-error' : '' }}' data-show-content="list;subcategory;url;empty;child-link">
        {!! Form::i18nInput("url", trans('dynamicpages::categories.fields.url'), $errors, $lang, $category, !$category ? ["data-slug" => "target"] : []) !!}
    </div>
    <div class="hide form-group" data-show-content="list;subcategory">
    	{!! Form::i18nTextarea("description", trans('dynamicpages::categories.fields.description'), $errors, $lang, $category) !!}
    </div>
</div>
