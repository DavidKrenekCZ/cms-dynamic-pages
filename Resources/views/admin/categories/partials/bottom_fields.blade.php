                    <div class="box-body hide cat-only icon-box">
                        {!! Form::label('icon_file', trans("dynamicpages::categories.fields.icon")); !!}
                        {!! Form::file('icon_file') !!}
                        @if ($category && trim($category->icon) != "")
                            <a href="{{ $category->iconPath }}" target="_blank"><img src="{{ $category->iconPath }}" class="mainImageThumbnail"></a>
                        @endif
                    </div>


@push('js-stack')
    <script src="{{ url("/modules/dynamicpages/assets/sumoselect/sumoselect.js") }}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "@php route('admin.dynamicpages.category.index') @endphp" }
                ]
            });

            $(".cat-only").toggleClass("hide", $('select[name="parent_id"]').val() == "null");
            $('select[name="parent_id"]').change(function() {
                $(".cat-only").toggleClass("hide", $(this).val() == "null");
            });

            $("select[name='content_type']").change(toggleContent);
            toggleContent();

            $(".sumoselect").SumoSelect({
                search: true,
                searchText: "{{ trans("dynamicpages::records.form.search") }}",
                noMatch : '{{ trans("dynamicpages::records.form.no match") }}',
            });
        });

        function toggleContent() {
            $("[data-show-content]").addClass("hide").each(function() {
                if ($(this).data("show-content").split(";").indexOf($("select[name='content_type']").val()) != -1)
                    $(this).removeClass("hide");
            });
        }
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush


@push("css-stack")
    <link rel="stylesheet" type="text/css" href="{{ url("/modules/dynamicpages/assets/sumoselect/sumoselect.css") }}">
    <style type="text/css">
        div[data-template-only] .checkbox {
            display: inline-block;
            margin-right: 2rem;
        }

        .SumoSelect {
            width: 100%;
            display: block;
        }

        .SumoSelect>.CaptionCont {
            border-color: #d2d6de;
        }

        .SumoSelect .select-all.partial>span i, .SumoSelect .select-all.selected>span i, .SumoSelect>.optWrapper.multiple>.options li.opt.selected span i {
            background-color: #3c8dbc;
        }

        .SumoSelect>.optWrapper>.options li.group>label {
            padding: 10px 15px 0;
            margin: 0;
        }

        .SumoSelect .select-all>span, .SumoSelect>.optWrapper.multiple>.options li.opt span {
            margin-left: -30px;
        }

        .SumoSelect>.optWrapper.multiple>.options li ul li.opt {
            padding-left: 45px;
        }

        .SumoSelect .select-all>label, .SumoSelect>.CaptionCont, .SumoSelect>.optWrapper>.options li.opt label {
            margin-bottom: 0;
            font-weight: 500;
        }
    </style>
@endpush