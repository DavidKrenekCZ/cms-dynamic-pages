                        <div class="form-group{{ $errors->has("parent_id") ? ' has-error' : '' }}">
                            <label>{{ trans('dynamicpages::categories.fields.parent') }}</label>
                            <select class="form-control" name="parent_id">
                                <option value="null">{{ trans('dynamicpages::categories.form.no parent') }}</option>
                                @foreach(\Modules\DynamicPages\Entities\Category::allMain() as $cat)
                                <option value="{{ $cat->id }}" {{ old("parent_id") == $cat->id || ($category && $category->parent_id == $cat->id) ? 'selected' : '' }}{{ (($category && $cat->id == $category->id) || $cat->content_type == "url" || $cat->content_tpy == "list") ? " disabled" : "" }}>
                                    {{ str_repeat("- ", $cat->depth-1) }} {{ $cat->name }} 
                                    @if ($cat->content_type == "url") (URL)@endif
                                    @if ($cat->content_type == "list") ({{ trans("dynamicpages::categories.fields.content_type_values.list") }})@endif
                                </option>
                                @endforeach
                            </select>
                        </div>

                        {!! Form::normalSelect("content_type", trans("dynamicpages::categories.fields.content_type"), $errors, $isAdmin ? [
                            "record" => trans("dynamicpages::categories.fields.content_type_values.record"), 
                            "list" => trans("dynamicpages::categories.fields.content_type_values.list"),
                            "subcategory" => trans("dynamicpages::categories.fields.content_type_values.subcategory"),
                            "url" => trans("dynamicpages::categories.fields.content_type_values.url"),
                            "empty" => trans("dynamicpages::categories.fields.content_type_values.empty"),
                            "homepage" => trans("dynamicpages::categories.fields.content_type_values.homepage"),
                            "link" => trans("dynamicpages::categories.fields.content_type_values.link"),
                            "child-link" => trans("dynamicpages::categories.fields.content_type_values.child-link"),
                            ] : [
                            "record" => trans("dynamicpages::categories.fields.content_type_values.record"), 
                            "list" => trans("dynamicpages::categories.fields.content_type_values.list"),
                            "url" => trans("dynamicpages::categories.fields.content_type_values.url"),
                            "empty" => trans("dynamicpages::categories.fields.content_type_values.empty"),
                            "homepage" => trans("dynamicpages::categories.fields.content_type_values.homepage"),
                            "link" => trans("dynamicpages::categories.fields.content_type_values.link"),
                            "child-link" => trans("dynamicpages::categories.fields.content_type_values.child-link"),
                            ], $category) !!}

                        <div data-show-content="record">
                            {!! Form::normalSelect("content_id_record", trans("dynamicpages::categories.fields.content_id_record"), $errors, \Modules\DynamicPages\Entities\Record::allArray(), $category, ["class" => "sumoselect"]) !!}
                        </div>
                        <div data-show-content="list;subcategory">
                            {!! Form::normalSelect("content_id_list", trans("dynamicpages::categories.fields.content_id_list"), $errors, \Modules\DynamicPages\Entities\Template::allArray(), $category, ["class" => "sumoselect"]) !!}
                        </div>

                        <div data-show-content="link;child-link">
                            {!! Form::normalSelect("content_id_link", trans("dynamicpages::categories.fields.content_id_link"), $errors, \Modules\DynamicPages\Entities\Category::allSelectArray(), $category, ["class" => "sumoselect"]) !!}
                        </div>