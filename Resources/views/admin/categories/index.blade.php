@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('dynamicpages::categories.title.categories') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('dynamicpages::categories.title.categories') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.dynamicpages.category.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('dynamicpages::categories.button.create category') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="position: relative">
                    <div class="table-responsive">
                        <table class="table-striped table table-bordered table-hover sortable">
                            <thead>
                            <tr data-parent-id="0">
                                <th>{{ trans('dynamicpages::categories.table.name') }}</th>
                                <th>{{ trans('dynamicpages::records.fields.url') }}</th>
                                <th>{{ trans('dynamicpages::categories.fields.content_type') }}</th>
                                <th>{{ trans('dynamicpages::categories.table.number of children') }}/{{ trans('dynamicpages::categories.fields.icon') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($categories)): ?>
                            <?php foreach ($categories as $category): ?>
                            <tr data-parent-id="{{ $category->parent_id or "0" }}" data-id="{{ $category->id }}">
                                <td>
                                    @if (!$category->parent)
                                    <span class="cat-hide-button">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                        <i class="fa fa-plus hide" aria-hidden="true"></i>
                                    </span>
                                    @endif
                                    <a href="{{ route('admin.dynamicpages.category.edit', [$category->id]) }}">
                                        {{ str_repeat("&nbsp;", ($category->depth-1)*15) }} {{ $category->name }}
                                    </a>
                                </td>
                                <td>
                                    {{ $category->url() }}
                                </td>
                                <td>
                                    {{ trans("dynamicpages::categories.fields.content_type_values.".$category->content_type) }}
                                </td>
                                <td>
                                    @if (!$category->parent)
                                        {{ $category->children->count() }}
                                    @else
                                        @if ($category->hasIcon())
                                        <a href="{{ $category->iconPath }}" target="_blank">
                                            <img src="{{ $category->iconPath }}" class="icon-thumb">
                                        </a>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.dynamicpages.category.edit', [$category->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.dynamicpages.category.destroy', [$category->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                    <div class="cat-preloader">
                        <div class="el-loading-spinner spinner"><svg viewBox="25 25 50 50" class="circular"><circle cx="50" cy="50" r="20" fill="none" class="path"></circle></svg><!----></div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('dynamicpages::categories.title.create category') }}</dd>
    </dl>
@stop

@push("css-stack")
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
    <style type="text/css">
        .ui-state-highlight {
            border: 1px solid #dad55e !important;
            background: #fffa90 !important;
            color: #777620 !important;
        }

        .ui-state-highlight-disabled {
            background: #b4b4b4 !important;
        }

        tr.ui-sortable-handle {
            cursor: move;
        }

        .cat-preloader {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255,255,255,.5)
        }

        .icon-thumb {
            width: 25px;
            height: 25px;
            border: 1px solid black;
            border-radius: 3px;
        }

        .cat-hide-button {
            cursor: pointer; 
            padding: 1px 10px 0 5px; 
            display: block; 
            float: left;
        }
    </style>
@endpush

@push('js-stack')
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $("table.table-striped.table-bordered.sortable tbody").sortable({
                placeholder: "ui-state-highlight",

                // Element dropped
                update: function(e, ui) {
                    // Dragging is not valid - remove
                    if ($(ui.placeholder).hasClass("ui-state-highlight-disabled"))
                        return false;

                    // Sort all subcategories after their parents
                    $(".sortable tr[data-parent-id=0]").each(function() {
                        var el = $(this);
                        var children = $("tr[data-parent-id='"+el.data("id")+"']");
                        for (var i = children.length - 1; i >= 0; i--)
                            $(children[i]).insertAfter(el);
                    });

                    var data = {};
                    $(".sortable tr[data-id]").each(function() {
                        var id = $(this).data("id");
                        var position = $(this).index()+1;
                        data[id] = position;
                    });

                    $(".cat-preloader").show();
                    $.ajax("{{ route("admin.dynamicpages.category.change-position") }}", {
                        type: "post",
                        data: {
                            _token: "{{ csrf_token() }}",
                            positions: data
                        }
                    }).fail(function(e) {
                        console.log(e);
                        alert("{{ trans('dynamicpages::categories.table.ajax error') }}");
                    }).always(function() {
                        $(".cat-preloader").hide();
                    });
                },

                // Element dragged, but not dropped
                change: function(e, ui) {
                    var el = $(ui.placeholder);                     // drop placeholder
                    var parent_id = $(ui.item).data("parent-id");   // ID of parent of dragged element
                    var id = $(ui.item).data("id");                 // ID of dragged element

                    // Next and previous rows
                    var nx = el.next("tr");
                    var pv = el.prev("tr");

                    // Subcategory is not next to sibling subcategories -> disable placeholder
                    if (
                        parent_id != 0 &&
                        nx.data("parent-id") != parent_id && pv.data("parent-id") != parent_id
                    )
                        el.addClass("ui-state-highlight-disabled");
                    else
                        el.removeClass("ui-state-highlight-disabled");
                },

                // Sorting starts
                start: function(e, ui) {
                    // If we're sorting main categories, hide subcategories
                    if ($(ui.item).data("parent-id") == 0)
                        $(".sortable tr[data-parent-id][data-parent-id!=0]").addClass("hide");
                },

                // Sorting stops
                stop: function(e, ui) {
                    // Show all subcategories
                    $(".sortable tr[data-parent-id][data-parent-id!=0]").removeClass("hide");
                }
            });

            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "@php route('admin.dynamicpages.category.create') @endphp" }
                ]
            });

            $(".cat-hide-button").click(function() {
                $("tr[data-parent-id='"+$(this).parent().parent().data("id")+"']").children().slideToggle(100);
                $(this).find(".fa").toggleClass("hide");
            }).each(function() { $(this).click() })
        });
    </script>
    @php $locale = locale(); @endphp
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "pageLength": 25,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '{{ url("/public/modules/core/js/vendor/datatables/{$locale}.json") }}'
                }
            });
        });
    </script>
@endpush
