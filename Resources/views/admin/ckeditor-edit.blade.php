@push("js-stack")
	<script type="text/javascript">
		$(".ckeditor").each(function() {
     		CKEDITOR.replace($(this).attr("id"), {
		    	extraPlugins: '{{ str_replace("Modules/Dynamicpages/Assets/_ckplugins/", "", implode(",", glob("Modules/Dynamicpages/Assets/_ckplugins/*", GLOB_ONLYDIR))) }}', // load extra plugins
		    	removeButtons: 'About'	// hide "About" button
			});
		});
	</script>
@endpush