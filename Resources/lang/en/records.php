<?php

return [
    'list resource' => 'List records',
    'create resource' => 'Create records',
    'edit resource' => 'Edit records',
    'destroy resource' => 'Destroy records',
    'title' => [
        'records' => 'Records',
        'create record' => 'Create a record',
        'edit record' => 'Edit a record',
    ],
    'button' => [
        'create record' => 'Create a record',
        "save visible" => "Save and publish",
        "save invisible" => "Save as concept",
        "delete selected" => "Delete selected",
        "update and stay" => "Update and stay",
        "update and leave" => "Update and leave",
        "clear cache"   => "Clear filter"
    ],
    'table' => [
        "created at" => "Created at",
        "visible" => "Visible",
        "template filter" => "Template filter",
        "category filter" => "Category filter",
        "no filter" => "(no filter)",
        "show" => "Show",
        "hide" => "Hide"
    ],
    'form' => [
        "search" => "Search...",
        "no match" => 'No matches for "{0}"',
        "caption format" => '{0} selected',
        "caption format all selected" => 'All {0} selected'
    ],
    'messages' => [
        "visibility toggled" => "Visibility successfully changed!"
    ],
    'validation' => [
        "url" => "A record with passed URL (:lang) already exists, please choose different URL"
    ],
    "fields" => [
        "name" => "Name",
        "url" => "URL",
        "meta_title" => "Meta title",
        "meta_description" => "Meta description",
        "sections" => "Show in sections",
        "categories" => "Categories",
        "published since" => "Published since",
        "published" => "Published",
        "published to" => "Published to",
        "no_published_to" => "Don't hide",
        "files_title" => "Attached files heading",
        "images_title" => "Photos heading"
    ],
    "default_files_title" => "Attached files",
    "default_images_title" => "Gallery"
];
