<?php

return [
    'list resource' => 'List categories',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Destroy categories',
    'title' => [
        'categories' => 'Categories',
        'create category' => 'Create a category',
        'edit category' => 'Edit a category',
    ],
    'button' => [
        'create category' => 'Create a category',
    ],
    'table' => [
        "path" => "Path",
        "name" => "Name",
        "ajax error" => "An error has occured, please try again",
        "number of children" => "Number of subcategories"
    ],
    'form' => [
        "no parent" => "(no parent)"
    ],
    'messages' => [
    ],
    'validation' => [
        "parent url" => "Selected parent category is of type \"An URL\".",
        "parent url change" => "This category has subcategories, therefore can't be set as type \"An URL\".",
        "parent list" => "Can't insert \"A list of resources\" category into a category of the same type.",
        "parent list change" => "This category has subcategories of \"A list of resources\" type, therefore can't be set to the same type.",
        "empty subcategory" => "\"No content\" can't be a subcategory.",
        "main category link" => "Can't link a category to a parent category.",
        "empty url" => "A default (".(config("asgard.dynamicpages.core.default_locale") ? config("asgard.dynamicpages.core.default_locale") : "cs").") URL can't be empty.",
        "empty name" => "A default (".(config("asgard.dynamicpages.core.default_locale") ? config("asgard.dynamicpages.core.default_locale") : "cs").") name can't be empty.",
        "child-link subcategory" => "\"Link to own subcategory\" can't be a subcategory.",
        "child-link not child" => "\"Link to own subcategory\" must link to it's own subcategory.",
        "invalid link" => "A category can't be linked to itself.",
        "content not admin" => "You don't have rights for this type of category's content.",
        "link to child" => "\"Link to category\" can't link to own subcategory - use \"Link to own subcategory\" instead.",
    ],
    "fields" => [
        "name" => "Name",
        "url" => "URL",
        "description" => "Description",
        "parent" => "Parent category",
        "icon" => "Icon",
        "content_type" => "Content",
        "content_type_values" => [
            "record" => "A record",
            "list" => "A list of records",
            "subcategory" => "Subcategory of list of records",
            "url" => "An URL",
            "empty" => "No content",
            "homepage" => "Homepage",
            "link" => "Link to categories",
            "child-link" => "Link to own subcategory"
        ],
        "content_id_record" => "Record",
        "content_id_list" => "Template",
        "content_id_link" => "Linked category",
    ],    
];
