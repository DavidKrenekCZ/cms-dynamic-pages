<?php

return [
    'list resource' => 'List templates',
    'create resource' => 'Create templates',
    'edit resource' => 'Edit templates',
    'destroy resource' => 'Destroy templates',
    'title' => [
        'template' => 'Template',
        'templates' => 'Templates',
        'create template' => 'Create a template',
        'edit template' => 'Edit a template',
    ],
    'button' => [
        'create template' => 'Create a template',
        "add field" => "Add another field"
    ],
    'table' => [
        "number of fields" => "Number of fields"
    ],
    'form' => [
        "name"  => "Name",
        "settings_files" => "File uploading",
        "settings_images" => "Photos uploading",
        "settings_url" => "URL setting",
        "settings_meta" => "Meta tags setting",
        "field name" => "Field name",
        "field type" => "Type",
        "field system name" => "System name",
        "field position" => "Position",
        "field remove" => "Delete",
        "types" => [
            "input" => "One row input",
            "textarea" => "Multi row input",
            "wysiwyg" => "Multi row formatted input",
            "date" => "Date input",
            "checkbox" => "Checkbox",
            "tags" => "Tags",
            "ac-tags" => "Autocomplete tags" 
        ],
        "category" => "Choose category",
        "main_image" => "Main image"
    ],
    'messages' => [
        "template undeletable" => "Template is used by some record therefore can't be deleted"
    ],
    'validation' => [
    ],
];
