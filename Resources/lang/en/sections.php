<?php

return [
    'list resource' => 'List sections',
    'create resource' => 'Create sections',
    'edit resource' => 'Edit sections',
    'destroy resource' => 'Destroy sections',
    'title' => [
        'sections' => 'Sections',
        'create section' => 'Create a section',
        'edit section' => 'Edit a section',
    ],
    'button' => [
        'create section' => 'Create a section',
    ],
    'table' => [
        "show field" => "Show"
    ],
    'form' => [
        "settings_files" => "Attached files",
        "settings_images" => "Photos",
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
