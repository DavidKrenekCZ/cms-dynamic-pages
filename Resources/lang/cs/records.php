<?php

return [
    'list resource' => 'Prohlížet záznamy',
    'create resource' => 'Vytvářet záznamy',
    'edit resource' => 'Upravovat záznamy',
    'destroy resource' => 'Mazat záznamy',
    'title' => [
        'records' => 'Záznamy',
        'create record' => 'Vytvořit záznam',
        'edit record' => 'Upravit záznam',
    ],
    'button' => [
        'create record' => 'Vytvořit záznam',
        "save visible" => "Uložit a publikovat",
        "save invisible" => "Uložit jako koncept",
        "delete selected" => "Smazat vybrané",
        "update and stay" => "Uložit a zůstat",
        "update and leave" => "Uložit a odejít",
        "clear cache"   => "Resetovat filtr"
    ],
    'table' => [
        "created at" => "Vytvořeno",
        "visible" => "Viditelné",
        "template filter" => "Filtr šablon",
        "category filter" => "Filtr kategorií",
        "no filter" => "(bez filtru)",
        "show" => "Zobrazit",
        "hide" => "Skrýt"
    ],
    'form' => [
        "search" => "Vyhledávání...",
        "no match" => 'Pro "{0}" nebyly nalezeny žádné výsledky',
        "caption format" => '{0} vybráno',
        "caption format all selected" => '{0} vybráno (vše)'
    ],
    'messages' => [
        "visibility toggled" => "Viditelnost úspěšně změněna!"
    ],
    'validation' => [
        "url" => "Záznam s vybranou URL nebo názvem (:lang) už existuje, změňte prosím název nebo URL"
    ],
    "fields" => [
        "name" => "Název",
        "url" => "URL",
        "meta_title" => "Meta titulek",
        "meta_description" => "Meta description",
        "sections" => "Zobrazit v sekcích",
        "categories" => "Kategorie",
        "published since" => "Datum publikování",
        "published" => "Publikováno",
        "published to" => "Publikováno do",
        "no_published_to" => "Neskrývat",
        "files_title" => "Nadpis u přiložených souborů",
        "images_title" => "Nadpis u fotek"
    ],
    "default_files_title" => "Přiložené soubory",
    "default_images_title" => "Fotogalerie"
];
