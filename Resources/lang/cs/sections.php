<?php

return [
    'list resource' => 'Prohlížet sekce',
    'create resource' => 'Vytvářet sekce',
    'edit resource' => 'Upravovat sekce',
    'destroy resource' => 'Mazat sekce',
    'title' => [
        'sections' => 'Sekce',
        'create section' => 'Vytvořit sekci',
        'edit section' => 'Upravit sekci',
    ],
    'button' => [
        'create section' => 'Vytvořit sekci',
    ],
    'table' => [
        "show field" => "Zobrazit"
    ],
    'form' => [
        "settings_files" => "Přiložené soubory",
        "settings_images" => "Fotky",
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
