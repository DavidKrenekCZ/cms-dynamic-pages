<?php

return [
    'list resource' => 'Prohlížet šablony',
    'create resource' => 'Vytvářet šablony',
    'edit resource' => 'Upravovat šablony',
    'destroy resource' => 'Mazat šablony',
    'title' => [
        'template' => 'Šablona',
        'templates' => 'Šablony',
        'create template' => 'Vytvořit šablonu',
        'edit template' => 'Upravit šablonu',
    ],
    'button' => [
        'create template' => 'Vytvořit šablonu',
        "add field" => "Přidat políčko"
    ],
    'table' => [
        "number of fields" => "Počet políček"
    ],
    'form' => [
        "name"  => "Název",
        "settings_files" => "Nahrávání souborů",
        "settings_images" => "Nahrávání fotek",
        "settings_url" => "Nastavení URL",
        "settings_meta" => "Nastavení meta tagů",
        "field name" => "Název políčka",
        "field type" => "Typ",
        "field system name" => "Systémový název",
        "field position" => "Pozice",
        "field remove" => "Odebrat",
        "types" => [
            "input" => "Řádek s textem",
            "textarea" => "Textové pole bez možností formátování",
            "wysiwyg" => "Textové pole s možnostmi formátování",
            "date" => "Datum",
            "checkbox" => "Zaškrtávací políčko",
            "tags" => "Tagy",
            "ac-tags" => "Tagy s našeptávačem" 
        ],
        "category" => "Výběr kategorie",
        "main_image" => "Hlavní obrázek"
    ],
    'messages' => [
        "template undeletable" => "Tuto šablonu využívá nějaký záznam, nemůže tudíž být odstraněna"
    ],
    'validation' => [
    ],
];
