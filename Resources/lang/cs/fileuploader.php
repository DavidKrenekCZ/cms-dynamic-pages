<?php
return [
    "button" => "function(options) { return 'Vybrat ' + (options.limit == 1 ? 'soubor' : 'soubory'); }",
    "feedback" => "function(options) { return 'Vybrat ' + (options.limit == 1 ? 'soubor' : 'soubory') + ' k nahrání'; }",
    "feedback2" => "function(options) { var ln = options.length; return 'Vybrán' + (ln != 1 ? ( ln > 1 && ln < 5 ? 'y' : 'o'  ) : '') + ' ' + options.length + ' soubor' + (ln != 1 ? ( ln > 1 && ln < 5 ? 'y' : 'ů'  ) : ''); }",    
    "confirm" =>  "Potvrdit",
    "cancel" =>  "Zrušit",
    "name" =>  "Název",
    "type" =>  "Typ",
    "size" =>  "Velikost",
    "dimensions" =>  "Rozměry",
    "duration" =>  "Trvání",
    "crop" =>  "Oříznout",
    "rotate" =>  "Otočit",
    "sort" =>  "Přesunout",
    "download" =>  "Stáhnout",
    "remove" =>  "Odstranit",
    "drop" =>  "Pro nahrání souborů je přetáhněte sem...",
    "paste" =>  "<div class='fileuploader-pending-loader'></div> Vkládám soubor, pro zrušení klikněte sem....",
   	"removeConfirmation" =>  "Opravdu chcete smazat tento soubor?",
    "errors" =>  [
        "filesLimit" =>  "Můžete nahrát pouze \${limit} souborů.",
        "filesType" =>  "Můžete nahrát pouze soubory \${extensions}.",
        "fileSize" =>  "Soubor \${name} je příliš velký! Vyberte prosím soubor velký maximálně \${fileMaxSize}MB.",
        "filesSizeAll" =>  "Vybrané soubory jsou příliš velké! Vyberte prosím soubory velké maximálně \${maxSize} MB.",
        "fileName" =>  "Soubor s názvem \${name} už je nahraný.",
        "folderUpload" =>  "Složky nelze nahrávat."
    ]
];