<?php

return [
    'list resource' => 'Prohlížet kategorie',
    'create resource' => 'Vytvářet kategorie',
    'edit resource' => 'Upravovat kategorie',
    'destroy resource' => 'Mazat kategorie',
    'title' => [
        'categories' => "Kategorie",
        'create category' => 'Vytvořit kategorii',
        'edit category' => 'Upravit kategorii',
    ],
    'button' => [
        'create category' => 'Vytvořit kategorii',
    ],
    'table' => [
        "path" => "Cesta",
        "name" => "Název",
        "ajax error" => "Vyskytla se chyba, opakujte akci",
        "number of children" => "Počet podkategorií"
    ],
    'form' => [
        "no parent" => "(žádná nadřazená)"
    ],
    'messages' => [
    ],
    'validation' => [
        "parent url" => "Do kategorie nelze vložit podkategorie, jedná se totiž o konkrétní url adresu.",
        "parent url change" => "Kategorie obsahuje podkategorie, nelze tedy nastavit obsah typu URL.",
        "parent list" => "Do kategorie s výpisem záznamů nelze vložit podkategorie stejného typu.",
        "parent list change" => "Kategorie obsahuje podkategorie typu výpis záznamů, nelze tedy nastavit obsah stejného typu.",
        "empty subcategory" => "\"Bez obsahu\" může být kategorie pouze 1. úrovně, ne jako podkategorie.",
        "main category link" => "Odkaz na kategorii může být pouze podkategorie určité kategorie.",
        "empty url" => "Nebyla vyplněna výchozí (".(config("asgard.dynamicpages.core.default_locale") ? config("asgard.dynamicpages.core.default_locale") : "cs").") URL.",
        "empty name" => "Nebyl vyplněn výchozí (".(config("asgard.dynamicpages.core.default_locale") ? config("asgard.dynamicpages.core.default_locale") : "cs").") název.",
        "child-link subcategory" => "\"Odkaz na vlastní podkategorii\" může být kategorie pouze 1. úrovně, ne jako podkategorie.",
        "child-link not child" => "\"Odkaz na vlastní podkategorii\" musí odkazovat na svou vlastní podkategorii.",        
        "invalid link" => "Kategorie nemůže odkazovat na sebe sama.",
        "content not admin" => "Pro tento typ obsahu kategorie nemáte právo.",
        "link to child" => "\"Odkaz na kategorii\" němůže odkazovat na vlastní podkategorii. Místo toho použijte typ \"Odkaz na vlastní podkategorii\"",
    ],
    "fields" => [
        "name" => "Název",
        "url" => "URL",
        "description" => "Popis",
        "parent" => "Nadřazená kategorie",
        "icon" => "Ikona",
        "content_type" => "Obsah",
        "content_type_values" => [
            "record" => "Konkrétní záznam",
            "list" => "Výpis záznamů",
            "subcategory" => "Podkategorie výpisu záznamů",
            "url" => "Konkrétní URL",
            "empty" => "Bez obsahu",
            "homepage" => "Úvodní strana",
            "link" => "Odkaz na kategorii",
            "child-link" => "Odkaz na vlastní podkategorii"
        ],
        "content_id_record" => "Záznam",
        "content_id_list" => "Dle šablony",
        "content_id_link" => "Odkazovaná kategorie",
    ],    
];
