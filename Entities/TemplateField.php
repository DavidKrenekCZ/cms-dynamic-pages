<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateField extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    protected $table = 'dynamicpages__templatefields';
    public $translatedAttributes = [
    	"name"
    ];
    protected $fillable = [
    	"position",
    	"type",
        "system_name",
    	"template_id"
    ];
    protected $dates = ['deleted_at'];
    protected $appends = ["formName"];

    public function getFormNameAttribute() {
        return 'templatefields_'.$this->template_id."_".$this->id;
    }



    /* RELATIONSHIPS */
    public function template() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Template");
    }



    /* METHODS */
    public function scopeOrdered($query) {
        return $query->orderBy('position', 'asc')->get();
    }
}