<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    protected $table = 'dynamicpages__sections';
    public $translatedAttributes = [
    	"name"
    ];
    protected $fillable = [
    	"settings_files",
    	"settings_images",
    	"template_id"
    ];
    protected $dates = ['deleted_at'];
    protected $appends = ["jsEditObject"];

    public function getJsEditObjectAttribute() {
        $names = [];
        $sectionsfields = [];
        $settings = [
            "files" => $this->settings_files,
            "images" => $this->settings_images
        ];

        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $lang)
            $names[$locale."[name]"] = $this->translate($locale)->name;

        foreach ($this->fields as $field)
            $sectionsfields[$field->template_field_id] = $field->show;

        return json_encode([
            "name" => $names,
            "sectionsfields" => $sectionsfields,
            "settings" => $settings
        ]);
    }



    /* RELATIONSHIPS */
    public function template() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Template");
    }

    public function fields() {
        return $this->hasMany("Modules\DynamicPages\Entities\SectionField");
    }
}
