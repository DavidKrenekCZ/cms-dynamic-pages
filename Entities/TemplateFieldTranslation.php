<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateFieldTranslation extends Model {
	use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
    	"name"
    ];
    protected $table = 'dynamicpages__templatefield_trs';
    protected $dates = ['deleted_at'];
}
