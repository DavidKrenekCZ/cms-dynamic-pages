<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryTranslation extends Model {
    use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
    	"name",
    	"url",
    	"description"
    ];
    protected $table = 'dynamicpages__category_translations';
    protected $dates = ['deleted_at'];
}
