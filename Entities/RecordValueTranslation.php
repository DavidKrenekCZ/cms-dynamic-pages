<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordValueTranslation extends Model {
	use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
    	"value"
    ];
    protected $table = 'dynamicpages__recordvalue_trs';
    protected $dates = ['deleted_at'];
}
