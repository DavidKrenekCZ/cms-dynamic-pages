<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UrlTranslation extends Model {
    public $timestamps = false;
    protected $fillable = [
    	"url"
    ];
    protected $table = 'dynamicpages__url_translations';
    public function urlModel() {
    	return $this->belongsTo("\Modules\DynamicPages\Entities\Url");
    }
}
