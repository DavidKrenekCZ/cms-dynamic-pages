<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateTranslation extends Model {
	use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
    	"name"
    ];
    protected $table = 'dynamicpages__template_translations';
    protected $dates = ['deleted_at'];
}
