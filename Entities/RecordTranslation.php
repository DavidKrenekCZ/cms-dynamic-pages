<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordTranslation extends Model {
	use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
    	"name",
    	"url",
    	"meta_description",
      "meta_title",
      "files_title",
      "images_title"
    ];
    protected $table = 'dynamicpages__record_translations';
    protected $dates = ['deleted_at'];

   	public function record() {
   		return $this->belongsTo("\Modules\DynamicPages\Entities\Record");
   	}

    /*
    public function getFilesTitleAttribute($value) {
      if (trim($value) == "")
        return trans("dynamicpages::records.default_files_title", [], $this->locale);
      return $value; 
    }
    
    public function getImagesTitleAttribute($value) {
      if (trim($value) == "")
        return trans("dynamicpages::records.default_images_title", [], $this->locale);
      return $value; 
    }
    */
}
