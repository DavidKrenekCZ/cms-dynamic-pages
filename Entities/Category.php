<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    protected $table = 'dynamicpages__categories';
    public $translatedAttributes = [
        "name",
        "url",
        "description"
    ];
    protected $fillable = [
        "parent_id",
        "icon",
        "content_type",
        "content_id"
    ];
    protected $dates = ['deleted_at'];

    protected $appends = ["depth", "iconPath", "link", "parentlessLink", "thumbnail", "content_id_category", "content_id_link", "content_id_record", "content_id_list", "content_list", "content_category", "content_record"];

    public function parent() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Category", "parent_id");
    }

    public function getDepthAttribute() {
        $x = $this->parent;
        $depth = 1;
        while ($x != false) {
            $x = $x->parent;
            $depth++;
        }
        return $depth;
    }

    public function getIconPathAttribute() {
        return $this->hasIcon() ? url("modules/dynamicpages/".$this->icon) : false;
    }

    public function getLinkAttribute() {
        $locale = count(config("laravellocalization.supportedLocales")) > 1 ? locale()."/" : "";
        if (!$this->parent)
            return url($locale.$this->url);
        return url($locale.$this->parent->url."/".$this->url);
    }

    public function getParentlessLinkAttribute() {
        $locale = count(config("laravellocalization.supportedLocales")) > 1 ? locale()."/" : "";
        return url($locale.$this->url);
    }

    public function getThumbnailAttribute() {
        return url("/modules/dynamicpages/".$this->icon);
    }

    public function getContentIdCategoryAttribute() { return $this->content_id; } 
    public function getContentIdRecordAttribute() { return $this->content_id; }
    public function getContentIdListAttribute() { return $this->content_id; }
    public function getContentIdLinkAttribute() { return $this->content_id; }

    public function getContentCategoryAttribute() {
        if ($this->content_type != "link" && $this->content_type != "child-link")
            return null;
        return Category::findOrFail($this->content_id);
    }

    public function getContentRecordAttribute() {
        if ($this->content_type != "record")
            return null;
        return Record::findOrFail($this->content_id);
    }

    public function getContentListAttribute() {
        if ($this->content_type != "list" && $this->content_type != "subcategory")
            return null;

        return Record::getByCategory($this->id)->filter(function($item) {
            return $item->template_id == $this->content_id;
        });
    }


    // Return name translated to default locale if selected is not present
    public function getNameAttribute($val) {
        return $this->attrOrDefault($val, "name"); }

    public function getUrlAttribute($val) {
        return $this->attrOrDefault($val, "url"); }

    private function attrOrDefault($val, $attr) {
        if (trim($val) != "")
            return $val;

        $locale = config("asgard.dynamicpages.core.default_locale");
        if (!$locale)
            $locale = "cs";

        return $this->translate($locale)->$attr;
    }

    /* RELATIONSHIPS */
    protected $with = [ "urls.translations" ];
    public function urls() {
        return $this->hasMany("Modules\DynamicPages\Entities\Url")->where("record_id", null)->relationshipOrdered();
    }

    public function children() {
        return $this->hasMany("Modules\DynamicPages\Entities\Category", "parent_id")->orderBy("position");
    }



    /* METHODS */
    public function isActive($children=true, $compareWith=false, $allowOnlyPart=false) {
        if ($children) {
            $children = $this->content_type == "link" ? self::where("parent_id", $this->content_id)->get() : $this->children;

            foreach ($children as $child) {
                if ($child->isActive(true, $compareWith))
                    return true;
            }
        }
        return $this->urls->first() && $this->urls->first()->isActive($compareWith, $allowOnlyPart);
    }
    
    public function url($useLocale=true) {
        $url = $this->urls->first();
        return $url ? ($useLocale && count(\LaravelLocalization::getSupportedLocales()) > 1 ? locale()."/" : "").($url->url) : "";
    }

    public function purl($prefix="") {
        return url((count(\LaravelLocalization::getSupportedLocales()) > 1 ? locale()."/" : "").$prefix.$this->url(0));
    }

    public static function allMain() {
        return self::where("parent_id", null)->with("children")->orderBy("position")->get();
    }

    // Returns category that has this record as its whole content
    public function linkingCategories() { 
        $categories = Category::where([["content_type", "link"], ["content_id", $this->id]]);
        return $categories->get();
    }

    public static function selectableArray() {
        $structured = [];
        foreach (self::allMain() as $main) {
            $children = $main->children->where("content_type", "list");

            if ($main->content_type == "list" || $children->count())
                $structured[$main->name] = [];

            if ($main->content_type == "list")
                $structured[$main->name][$main->id] = $main->name;

            foreach ($children as $value)
                $structured[$main->name][$value->id] = $value->name;
        }
        return $structured;
    }    

    public static function structuredAll() {
        $structured = [];
        foreach (self::allMain() as $main) {
            $structured[] = $main;
            foreach ($main->children as $value)
                $structured[] = $value;
        }
        return $structured;
    } 

    public static function filterArray() {
        $structured = [];
        foreach (self::allMain() as $main) {
            $children = $main->children->where("content_type", "list");

            if ($children->count()) {
                $structured[$main->name] = [];
                foreach ($children as $value)
                    $structured[$main->name][$value->id] = $value->name;
            }
        }
        return $structured;
    }

    public static function allArray() {
        $structured = [];
        foreach (self::allMain() as $main) {
            $structured[$main->name] = [];
            foreach ($main->children as $value)
                $structured[$main->name][$value->id] = $value->name;
        }
        return $structured;
    }

    public static function allSelectArray() {
        $structured = [];
        foreach (self::structuredAll() as $category) {
            $prepend = "&nbsp;&nbsp;&nbsp;";
            if ($category->parent)
                $prepend .= "&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;";

            $structured[$category->id] = $prepend.$category->name;
        }
        return $structured;
    }

    public function hasIcon() {
        return (trim($this->icon) != "" && file_exists(public_path("/modules/dynamicpages/".$this->icon)));
    }

    protected static function boot() {
        parent::boot();

        // Delete children categories when category is being deleted
        static::deleting(function($category) {
            self::where("parent_id", $category->id)->delete();
        });
    }
}
