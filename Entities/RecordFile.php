<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordFile extends Model {
    /* SETTINGS */
    use SoftDeletes;

    protected $table = 'dynamicpages__record_files';
    protected $fillable = [
        "name",
        "position",
        "record_id"
    ];
    protected $dates = ['deleted_at'];
    protected $appends = ["serverpath", "publicpath", "downloadpath"];

    public function getServerpathAttribute() {
        if (!$this->record)
            abort(404);
        return public_path("/modules/dynamicpagesrecords/files/".$this->record_id."/".$this->name);
    }
    
    public function getPublicpathAttribute() {
        if (!$this->record)
            abort(404);
        return url("/modules/dynamicpagesrecords/files/".$this->record_id."/".$this->name);
    }

    public function getDownloadPathAttribute() {
        return route("admin.dynamicpages.download-file", [
            "file" => $this
        ]);
    }

    public function getShowPathAttribute() {
        return route("admin.dynamicpages.show-file", [
            "file" => $this
        ]);
    }



    /* RELATIONSHIPS */
    public function record() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Record");
    }



    /* METHODS */
    public function scopeOrdered($query) {
        return $query->orderBy('position', 'asc')->get();
    }

    public function scopeRelationshipOrdered($query) {
        return $query->orderBy('position', 'asc');
    }

    public function remove() {
    	// Delete physical file if it exists
        if (file_exists($this->serverpath))
            unlink($this->serverpath);

        $this->delete();
    }
}
