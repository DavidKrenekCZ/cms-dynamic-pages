<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\DynamicPages\Entities\Record;

class RecordValue extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    protected $table = 'dynamicpages__recordvalues';
    public $translatedAttributes = [
    	"value"
    ];
    protected $fillable = [
    	"record_id",
    	"template_field_id"
    ];
    protected $dates = ['deleted_at'];

    protected $appends = ['acTagsSlugs', "acTagsModels", "noFallbackValue"];

    // Return autocomplete value as slugs of record names
    public function getAcTagsSlugsAttribute() {
        if ($this->field->type != "ac-tags")
            return "";

        $val = [];
        foreach(explode(",", $this->value) as $recordId) {
            $record = Record::find($recordId);
            if ($record)
                $val[] = $record->autocompleteSlug;
        }
        return implode(",", $val);   
    }

    // Return autocomplete value as Models
    public function getAcTagsModelsAttribute() {
        if ($this->field->type != "ac-tags")
            return [];

        $IDsArray = explode(",", $this->value);

        return Record::whereIn("id", $IDsArray)->get()->sortBy(function($item) use ($IDsArray) {
            return array_search($item->id, $IDsArray);
        });
    } 

    // Get translated value or the default one
    public function getValueAttribute() {
        $value = $this->translate(locale())->value;

        $fallbackLocale = config("translatable.fallback_locale");
        $useFallback = config("translatable.use_fallback");

        if (
            !$useFallback ||                // fallback disabled
            trim($value) != "" ||           // value set
            locale() == $fallbackLocale     // locale is already fallback
        )
            return $value;                  // return translated value

        // return fallback value
        return $this->translate($fallbackLocale)->value;
    }
    
    // If value is not present, don't return fallback
    public function getNoFallbackValueAttribute() {
    	return $this->translate(locale())->value;
	}


    /* RELATIONSHIPS */
    protected $with = ["field", "translations"];
    public function record() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Record");
    }

    public function field() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\TemplateField", "template_field_id");
    }

    public function translations() {
        return $this->hasMany("\Modules\DynamicPages\Entities\RecordValueTranslation");
    }
}
