<?php

namespace Modules\DynamicPages\Entities;
 
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\DynamicPages\Entities\Section;
use Modules\DynamicPages\Entities\Category;
use Modules\DynamicPages\Http\Controllers\Admin\FileUploader;
use Carbon\Carbon;
 
class Record extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    public $translationModel = "\Modules\DynamicPages\Entities\RecordTranslation";
    public $translationForeignKey = "record_id";

    protected $table = 'dynamicpages__records';
    public $translatedAttributes = [
        "name",
        "url",
        "meta_description",
        "meta_title",
        "files_title",
        "images_title"
    ];
    protected $fillable = [
        "template_id",
        "categories_array",
        "sections_array",
        "visible",
        "published_since",
        "published_to"
    ];
    protected $dates = ['deleted_at', "updated_at"];

    protected $appends = ["sections", "sectionsModels", "sectionsNames", "categoriesModels", "categoriesNames", "sectionsFormValues", "fieldsFormValues", "categories", "appendedFiles", "appendedImages", "appendedImagesUncached", "filesDir", "imagesDir", "serverMainImage", "mainImage", "firstImage", "pageTitle", "publicLink", "published_since", "published_to", "eventTime", "czechFilesCount", "autocompleteSlug"];

    public function getCzechFilesCountAttribute() {
        $n = $this->files->count();
        if ($n == 1)
            return "1 soubor";
        
        if ($n >= 2 && $n <= 4)
            return $n." soubory";
        
        return $n." souborů";
    }

    public function getSectionsAttribute() {
        return json_decode($this->sections_array);
    }

    public function getSectionsModelsAttribute() {
        $result = [];
        foreach ($this->sections as $section)
            $result[] = Section::find($section);
        return $result;
    }

    public function getSectionsNamesAttribute() {
        $result = [];
        foreach ($this->sectionsModels as $section)
            $result[] = $section->name;
        return $result;
    }

    public function getCategoriesAttribute() {
        if (trim($this->categories_array) == "")
            return [];
        return json_decode($this->categories_array);
    }

    public function getCategoriesModelsAttribute() {
        return Category::whereIn("id", $this->categories)->get();
    }

    public function getCategoriesNamesAttribute() {
        $result = [];
        foreach ($this->categoriesModels as $category)
            $result[] = $category->name;
        return $result;
    }

    public function getSectionsFormValuesAttribute() {
        $return = [];
        foreach ($this->sections as $section)
            $return["sections[$section]"] = 1;
        return (object)$return;
    }

    public function getFieldsFormValuesAttribute() {
        $result = [];
        
        $this->load("values.field.translations");
        foreach($this->values as $value)
            foreach (\LaravelLocalization::getSupportedLocales() as $locale => $lang)
                if ($value->field) {
                    $val = $value->translate($locale)->value;

                    if ($value->field->type == "ac-tags")
                        $val = $value->acTagsSlugs;

                    $result[$locale."[templatefields_".$value->field->template_id."_".$value->field->id."]"] = $val;
                }

        return $result;
    }

    public function getAppendedFilesAttribute() {
        return $this->appendedAttributes($this->files);
    }

    public function getAppendedImagesAttribute() {
        return $this->appendedAttributes($this->images);
    }

    private function appendedAttributes($files) {
        $appendedFiles = [];
        foreach($files as $file) {
            if (file_exists($file->serverpath))
                $appendedFiles[] = array(
                    "name" => $file->name,
                    "type" => FileUploader::mime_content_type($file->serverpath),
                    "size" => filesize($file->serverpath),
                    "file" => isset($file->originalImage) ? $file->originalImage->publicpath : $file->publicpath,
                    "data" => array(
                        "url" => isset($file->originalImage) ? $file->originalImage->publicpath : $file->publicpath
                    )
                );
            else
                $file->remove();
        }
        return json_encode($appendedFiles);
    }

    public function getImagesDirAttribute() {
        return public_path("modules/dynamicpagesrecords/images/".$this->id);
    }

    public function getFilesDirAttribute() {
        return public_path("modules/dynamicpagesrecords/files/".$this->id);
    }

    public function getServerMainImageAttribute() {
        $p = public_path("modules/dynamicpagesrecords/main_images/");
        foreach (["gif", "GIF", "jpg", "JPG", "png", "PNG"] as $ext)
            if (file_exists($p."/".$this->id.".".$ext))
                return $p."/".$this->id.".".$ext;
        return false;
    }

    public function getMainImageAttribute() {
        $p = public_path("modules/dynamicpagesrecords/main_images/");
        foreach (["gif", "GIF", "jpg", "JPG", "png", "PNG"] as $ext)
            if (file_exists($p."/".$this->id.".".$ext))
                return url("modules/dynamicpagesrecords/main_images/".$this->id.".".$ext."?updated=".floor(time()/100));
        
        if ($this->firstImage != "")
            return $this->firstImage;
        
        return false;
    }

    public function getPageTitleAttribute() {
        if ($this->meta_title && trim($this->meta_title) != "")
            return trim($this->meta_title);
        return $this->name;
    }

    public function getPublicLinkAttribute() {
        $locale = count(config("laravellocalization.supportedLocales")) > 1 ? locale()."/" : "";
        return url($locale.$this->url);
    }

    public function getPublishedSinceAttribute($value) {
        //\Modules\DynamicPages\Entities\Url::compose($this);
        if (trim($value) != "" && 
            (int)$value > 1000000 && 
            Carbon::createFromTimestamp($value))
            return Carbon::createFromTimestamp($value);
        return Carbon::now();
    }

    public function getPublishedToAttribute($value) {
        if ($value &&
            trim($value) != "" && 
            (int)$value > 1000000 && 
            Carbon::createFromTimestamp($value))
            return Carbon::createFromTimestamp($value);
        return false;
    }

    public function getFirstImageAttribute() {
        return $this->images->count() ? $this->images->first()->publicpath : "";
    }

    public function getEventTimeAttribute() {
        $d = $this->getFieldsByName("Datum konání", "cs");
        if ($d && $d->count())
            $date = trim($d->first()->value);
        else
            $date = "";

        $t = $this->getFieldsByName("Čas konání", "cs");
        if ($t && $t->count())
            $time = trim(substr(trim(str_replace(".", ":", $t->first()->value)), 0, 5));
        else
            $time = "";

        // check correct date format
        if (isset($date) && !empty($date)){
            try {
                Carbon::createFromFormat('d. m. Y', $date);
            } catch (\Exception $e) {
                $date = "";
            }
        }

        // check time format
        if (isset($time) && !empty($time)){
            try {
                Carbon::createFromFormat('H:i', $time);
            } catch (\Exception $e) {
                $time = "";
            }
        }

        // return value
        if ($date != "" && $time == "")
            return Carbon::createFromFormat("d. m. Y", $date);
        if ($date == "")
            return $this->published_since;
        return Carbon::createFromFormat("d. m. Y H:i", $date." ".$time);
    }

    public function getAutocompleteSlugAttribute() {
        return "#".$this->id." - ".str_limit($this->name, 15);
    }



    // Return attribute translated to default locale if selected is not present
    public function getNameAttribute($val) {
        return $this->attrOrDefault($val, "name"); }

    public function getMetaDescriptionAttribute($val) {
        return $this->attrOrDefault($val, "meta_description"); }

    public function getMetaTitleAttribute($val) {
        return $this->attrOrDefault($val, "meta_title"); }

    public function getFilesTitleAttribute($val) {
        return $this->attrOrDefault($val, "files_title"); }

    public function getImagesTitleAttribute($val) {
        return $this->attrOrDefault($val, "images_title"); }

    private function attrOrDefault($val, $attr) {
        if (trim($val) != "")
            return $val;

        $locale = config("asgard.dynamicpages.core.default_locale");
        if (!$locale)
            $locale = "cs";

        return $this->translate($locale)->$attr;
    }




    /* RELATIONSHIPS */
    protected $with = [ "translations", "values", "urls.translations" ];
    public function template() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Template");
    }

    public function values() {
        return $this->hasMany("Modules\DynamicPages\Entities\RecordValue", "record_id");
    }

    public function images() {
        return $this->hasMany("Modules\DynamicPages\Entities\RecordImage", "record_id")->relationshipOrdered();
    }

    public function files() {
        return $this->hasMany("Modules\DynamicPages\Entities\RecordFile", "record_id")->relationshipOrdered();
    }

    public function urls() {
        return $this->hasMany("Modules\DynamicPages\Entities\Url", "record_id")->relationshipOrdered();
    }

    /** 
     * Returns categories that have this record as their whole content
     *
     * @return Collection
     */
    public function slavingCategories() {
        return $this->belongsToMany("\Modules\DynamicPages\Entities\Category", "id", "content_id")->where("content_type", "record");
    }

    /** 
     * Returns category that has this record as its whole content
     *
     * @return Category
     */
    public function slavingCategory() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Category", "id", "content_id")->where("content_type", "record");
    }




    /* METHODS */
    /**
     * Check if URL is active
     *
     * @param mixed     $compareWith    string to compare Record's URL with (false = page URL)
     * @return bool
     */
    public function isActive($compareWith = false) {
        return $this->urls->first() && $this->urls->first()->isActive($compareWith);
    }

    /**
     * Get generated URL for record
     *
     * @param int       $category       ID of category to select URL from
     * @param bool      $useLocale      whether to prepend locale or not
     * @return string
     */
    public function url($category=false, $useLocale=true) {
        if (count($this->categories) < 2 || !$category)
            $url = $this->urls->first();
        else
            $url = $this->urls->where("category_id", $category)->first();
        return $url ? ($useLocale && count(\LaravelLocalization::getSupportedLocales()) > 1 ? locale()."/" : "").$url->url : "";
    }

    /**
     * Public url
     *
     * @param int|string    $category   (or prefix)
     */
    public function purl($category=false) {
        // Argument is URL prefix
        if ($category && is_string($category) && (int)$category <= 0)
            return url((count(\LaravelLocalization::getSupportedLocales()) > 1 ? locale()."/" : "").$category.$this->url(0, 0));

        // Argument is category ID
        return url($this->url($category));
    }

    /**
     * Get value of short description of record (annotation, description, ...)
     *
     * @param int       $chars  how many characters should the string be limitted to (default 130)
     * @return string
     */
    public function shortDesc($chars=130) {
        $text = "";

        foreach (["Anotace", "Popis", "Obsah", "Text"] as $field)
            if ($this->getFieldByName($field, "cs") && $text == "")
                $text = $this->getFieldByName($field, "cs")->value;
        
        $text = strip_tags(html_entity_decode($text));
        if (!$chars)
            return $text;
        return str_limit($text, $chars);
    }

    /**
     * Get value of long text of record (content, text, ...)
     *
     * @return string
     */
    public function longText() {
        foreach (["Obsah", "Text", "Popis", "Anotace"] as $field)
            if ($this->getFieldByName($field, "cs"))
                return $this->getFieldByName($field, "cs")->value;
    }

    /**
     * Return only visible records, ordered by published_since (desc)
     *
     * @return Collection
     */
    public function scopeOrdered($query) {
        return $query->where([["visible", 1], ["published_since", "<", time()]])->orderBy('published_since', 'desc')->orderBy('id', 'desc')->get();
    }

    /**
     * Get all visible records
     *
     * @return Collection
     */
    public static function allVisible() {
        return self::where([["visible", 1], ["published_since", "<", time()]])->get();
    }

    /**
     * Get all records in an array (id => name)
     *
     * @return array
     */
    public static function allArray() {
        $result = [];
        foreach (self::all() as $record)
            $result[$record->id] = $record->name;
        return $result;
    }

    /**
     * Get records in certain category
     *
     * @param int   $categoryId
     * @param bool  $visibleOnly = true
     * @return Collection
     */
    public static function getByCategory($categoryId, $visibleOnly=true) {
        if ($visibleOnly)
            $collection = self::allVisible();
        else
            $collection = self::all();
        return $collection->sortByDesc("published_since")->filter(function($item) use ($categoryId) {
            return in_array($categoryId, $item->categories);
        });
    }

    /**
     * Get records with certain template
     * @param int   $templateId
     * @param bool  $visibleOnly = true
     * @return Collection     
     */
    public static function getByTemplate($templateId, $visibleOnly=true) {
        $collection = self::where("template_id", $templateId)->orderByDesc("published_since");
        if ($visibleOnly)
            return $collection->where([["visible", 1], ["published_since", "<", time()]])->get();
        return $collection->get();
    }

    /**
     * Get all Record's fields values with certain type
     *
     * @param string    $ŧype
     * @return Collection
     */
    public function getFieldsByType($type) {
        return $this->values->filter(function($item) use ($type) {
            if (!$item->field)
                return false;
            return ($item->field->type == $type);
        });
    }

    /**
     * Get first Record's field value with certain type
     *
     * @param string    $ŧype
     * @return RecordValue
     */
    public function getFieldByType($type) {
        return $this->getFieldsByType($type)->first();
    }


    /**
     * Get all Record's fields values with certain name
     *
     * @param string    $name
     * @param string    $locale (default - default locale)
     * @return Collection
     */
    public function getFieldsByName($name, $locale=false) {
        if (!$locale)
            $locale = locale();

        return $this->values->filter(function($item) use ($locale, $name) {
            if (!$item->field)
                return false;
            $field = $item->field->translate($locale);

            if (!$field)
                return false;

            return (trim($field->name) == trim($name));
        });
    }

    /**
     * Get first Record's field value with certain name
     *
     * @param string    $name
     * @param string    $locale (default - default locale)
     * @return Collection
     */
    public function getFieldByName($name, $locale=false) {
        $field = $this->getFieldsByName($name, $locale)->first();
        if (!$field)
            return false;

        if (trim($field->value) == "")
            return $field->translate("cs");
        return $field;
    }

    /**
     * Get all Record's fields values with certain system name
     *
     * @param string    $systemName
     * @return Collection
     */
    public function getFieldsBySystemName($systemName) {
        return $this->values->filter(function($item) use ($systemName) {
            if (!$item->field)
                return false;

            if (mb_strlen($item->field->system_name))
                return ($item->field->system_name == $systemName);
            return ($item->field->name == $systemName);
        });
    }

    /**
     * Get first Record's field value with certain system name
     *
     * @param string    $systemName
     * @return RecordValue
     */
    public function getFieldBySystemName($systemName) {
        return $this->getFieldsBySystemName($systemName)->first();
    }

    /**
     * Get all fields of a record
     *
     * @param mixed     $except = []
     * @return array
     */
    public function getAllFields($except = []) {
        if (!$except)
            $except = [];
        elseif (!is_array($except))
            $except = [ $except ];

        return $this->values->filter(function($item) use($except) {
            return 
                $item->field && 
                (
                    (mb_strlen($item->field->system_name) && !in_array($item->field->system_name, $except)) ||
                    (!mb_strlen($item->field->system_name) && !in_array($item->field->name, $except))
                ); 
        });
    }

    /**
     * Check if Record is in category
     *
     * @param int   $categoryId
     * @return bool
     */
    public function isInCategory($categoryId) {
        return in_array($categoryId, json_decode($this->categories_array, 1));
    }
    
    /** 
     * Get Records by RecordValue's value
     *
     * @param string    $fieldName
     * @param string    $value
     * @param string    $locale     (default - default locale)
     * @param bool      $useSystemName (default - false)
     * @return Collection
     */
    public static function whereValue($fieldName, $value, $locale=false, $useSystemName=false) {
        $records = self::with("values.field.translations", "values.translations")->get();

        $records = $records->filter(function($record) use ($value, $locale, $fieldName, $useSystemName) {
            if (!$useSystemName)
                $field = $record->getFieldByName($fieldName, $locale);
            else
                $field = $record->getFieldBySystemName($fieldName);

            return ($field && $field->value == $value);
        });
        return $records;
    }

    /**
     * Returns parent category of record
     *
     * @return mixed
     */
    public function categoryOfList() {
        foreach ($this->categoriesModels as $category)
            if ($category->content_type == "list" && $category->content_id == $this->template_id)
                return $category;
        return false;
    }
}
