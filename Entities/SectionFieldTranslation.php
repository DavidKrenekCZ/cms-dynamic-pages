<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;

class SectionFieldTranslation extends Model {
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'dynamicpages__sectionfield_trs';
}
