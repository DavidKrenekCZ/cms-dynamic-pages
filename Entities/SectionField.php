<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectionField extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    public $timestamps = false;
    protected $table = 'dynamicpages__sectionfields';
    public $translatedAttributes = [];
    protected $fillable = [
    	"template_field_id",
    	"section_id",
    	"show"
    ];
    protected $dates = ['deleted_at'];



    /* RELATIONSHIPS */
    public function templateField() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\TemplateField");
    }

    public function section() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Section");
    }
}
