<?php

namespace Modules\DynamicPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectionTranslation extends Model {
	use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
    	"name"
    ];
    protected $table = 'dynamicpages__section_translations';
    protected $dates = ['deleted_at'];
}
