<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model {
    /* SETTINGS */
    use Translatable;
    use SoftDeletes;

    protected $table = 'dynamicpages__templates';
    public $translatedAttributes = [
    	"name"
    ];
    protected $fillable = [
    	"settings_files",
        "settings_images",
        "settings_url",
        "settings_meta",
        "category",
        "main_image"
    ];
    protected $dates = ['deleted_at'];
    protected $appends = ["settingsObject"];

    public function getSettingsObjectAttribute() {
        return json_encode([
            "settings_files" => $this->settings_files,
            "settings_images" => $this->settings_images,
            "settings_url" => $this->settings_url,
            "settings_meta" => $this->settings_meta,
            "category" => $this->category,
            "main_image" => $this->main_image
        ]);
    }



    /* RELATIONSHIPS */
    public function fields() {
        return $this->hasMany("Modules\DynamicPages\Entities\TemplateField");
    }

    public function sections() {
        return $this->hasMany("Modules\DynamicPages\Entities\Section");
    }

    public function records() {
        return $this->hasMany("Modules\DynamicPages\Entities\Record");
    }

    public function recordsVisible() {
        return $this->hasMany("Modules\DynamicPages\Entities\Record")->where("visible", 1);
    }




    /* METHODS */    
    public static function allArray() {
        $return = [];
        foreach (self::all() as $template)
            $return[$template->id] = $template->name;
        return $return;
    }
}
