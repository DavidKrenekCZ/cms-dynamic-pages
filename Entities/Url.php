<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Modules\DynamicPages\Entities\Record;
 
class Url extends Model {
    /* SETTINGS */
    use Translatable;

    protected $table = 'dynamicpages__urls';
    public $translatedAttributes = [
        "url"
    ];
    protected $fillable = [
        "category_id",
        "record_id",
        "canonical"
    ];



    /* RELATIONSHIPS */
    public function record() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Record");
    }

    public function category() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Category");
    }



    /* METHODS */
    /**
     * Check whether URL is currently approached or not
     *
     * @param string|bool   $compareWith    string to be compared with URL (default is Request path)
     * @param string|bool   $allowOnlyPart  accept as active also if it's just part of URL (e. g. "foo" would be active at "foo/bar")
     * @return bool
     */
    public function isActive($compareWith = false, $allowOnlyPart = false) {
        if ($this->record)
            $item = $this->record;
        elseif ($this->category)
            $item = $this->category;
        else
            return false;

        if (!$compareWith)
            $compareWith = \Request::decodedPath();

        return (
            // Homepage
                (
                    (trim($compareWith, "/") == "" || trim($compareWith, "/") == locale()) &&     // URL is empty or just locale
                    (trim($item->url(), "/") == "" || trim($item->url(), "/") == locale())                              // item URL is empty or just locale
                ) || 

            // Other pages
                (
                    (trim($item->url(), "/") != "" && trim($item->url(), "/") != locale()) &&                           // item URL is not empty or just locale
                    mb_substr(trim($compareWith, "/"), 0, mb_strlen($item->url())) == $item->url() &&        // item URL is same as URL
                    ($allowOnlyPart || mb_substr(trim($compareWith, "/"), mb_strlen($item->url()), 1) == "")                     // not just part of URL is correct
                )
        );
    }

    /**
     * Orders URLs by updated_at in descending order
     */
    public function scopeOrdered($query) {
        return $query->orderBy('canonical', 'desc')->orderBy('updated_at', 'desc')->get();
    }

    public function scopeRelationshipOrdered($query) {
        return $query->orderBy('canonical', 'desc')->orderBy('updated_at', 'desc');
    }

    public static function compose() {}

    /**
     * Composes an URL for given record/category
     * @param Record/Category   $item
     * @return boolean
     */
    public static function composeUrl($item) {
        $record = get_class($item) == "Modules\DynamicPages\Entities\Record";
        $update = false;

        if ($record) {
            // Record
            if (count($item->categories) > 1) {
                $updated = false;
                foreach ($item->categoriesModels as $cat) {
                    $update = false;
                    $data = [
                        "category_id"   => $cat->id,
                        "record_id"     => $item->id,
                        "canonical"     => 1
                    ];

                    $condition = [
                        ["category_id", $cat->id],
                        ["record_id", $item->id],
                    ];

                    // Check if identical URL doesnt already exist
                    $currentUrl = Url::where($condition)->ordered()->first();

                    foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
                        $url = trim(self::composeRecordUrl($item, $locale, $cat), "/");

                        if ((!$currentUrl || $currentUrl->translate($locale)->url != $url) && $url != "")
                            $update = true;
                       
                        $data[$locale] = [
                            "url"   => $url
                        ];
                    }

                    if ($update) {
                        if ($currentUrl)
                            $currentUrl->update([
                                "canonical" => 0
                            ]);

                        $url = new Url($data);
                        $url->save();
                        $updated = true;
                    }
                }

                if ($updated)
                    self::recomposeSlavingCategories($item);
                return true;
            } else {
                $data = [
                    "category_id"   => null,
                    "record_id"     => $item->id
                ];

                $condition = [
                    ["category_id", null],
                    ["record_id", $item->id],
                ];
            }
        } else {
            // Category
            $data = [
                "category_id"   => $item->id,
                "record_id"     => null
            ];

            $condition = [
                ["category_id", $item->id],
                ["record_id", null],
            ];
        }

        // Check if identical URL doesnt already exist
        $currentUrl = Url::where($condition)->ordered()->first();

        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
            if ($record)
                $url = trim(self::composeRecordUrl($item, $locale), "/");
            else
                $url = trim(self::composeCategoryUrl($item, $locale), "/");

            if ((!$currentUrl || $currentUrl->translate($locale)->url != $url) && ($url != "" || (!$record && $item->content_type == "homepage")))
                $update = true;
            $data[$locale] = [
                "url"   => $url
            ];
        }

        $data["canonical"] = 1;

        if ($update) {
            if ($currentUrl)
                $currentUrl->update([
                    "canonical" => 0
                ]);

            $url = new Url($data);
            $url->save();

            if ($record)
                self::recomposeSlavingCategories($item);
            else {
                self::recomposeLinkingCategories($item);
                self::recomposeCategorizedRecords($item);
                self::recomposeChildrenCategories($item);
            }
        }

        return true;
    }


    /**
     * Composes URLs of categories that have given record as content
     * @param Record    $record
     */
    private static function recomposeSlavingCategories($record) {
        foreach ($record->slavingCategories() as $category)
            Url::composeUrl($category);
    }

    /**
     * Composes URLs of categories that are linked to given category
     * @param Category  $category
     */
    private static function recomposeLinkingCategories($category) {
        foreach ($category->linkingCategories() as $cat)
            Url::composeUrl($cat);
    }

    /**
     * Composes URLs of categories that are children of given category
     * @param Category  $category
     */
    private static function recomposeChildrenCategories($category) {
        foreach ($category->children as $cat)
            Url::composeUrl($cat);
    }

    /**
     * Composes URLs of records that are categorized in given category
     * @param Category  $category
     */
    private static function recomposeCategorizedRecords($category) {
        foreach (Record::getByCategory($category->id, false) as $record)
            Url::composeUrl($record);
    }

    /**
     * Composes URL for given category and language
     * @param Category  $category
     * @param string    $lang
     * @return string
     */
    private static function composeCategoryUrl($category, $lang) {
        if ($category->content_type == "url" || $category->contnt_type == "child-link")
            return $category->translate($lang)->url;

        if ($category->content_type == "link")
            return $category->content_category->urls()->ordered()->first()->translate($lang)->url;

        if ($category->content_type == "homepage")
            return "";

        $url = [];
        if ($category->content_type == "record")
            array_unshift($url, $category->content_record->translate($lang)->url);
        else {
            if ($category->translate($lang)->url != "")
                array_unshift($url, $category->translate($lang)->url);
        }

        if ($category->parent && trim($category->parent->translate($lang)->url) != "")
            array_unshift($url, self::composeCategoryUrl($category->parent, $lang));

        return implode("/", $url);
    }

    /**
     * Composes URL for given record, language and category
     * @param Category  $category
     * @param string    $lang
     * @return string
     */
    private static function composeRecordUrl($record, $lang, $category=false) {
        if (count($record->categories)) {
            $category = (count($record->categories) == 1 || !$category) ? $record->categoriesModels[0] : $category;
            return self::composeCategoryUrl($category, $lang)."/".$record->id."-".$record->translate($lang)->url;
        }
        return $record->id."-".$record->translate($lang)->url;
    }
}