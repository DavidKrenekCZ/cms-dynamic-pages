<?php

namespace Modules\DynamicPages\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordImage extends Model {
    /* SETTINGS */
    use SoftDeletes;

    protected $table = 'dynamicpages__record_images';
    protected $fillable = [
    	"name",
        "position",
        "record_id"
    ];
    protected $dates = ['deleted_at'];
    protected $appends = ["serverpath", "publicpath", "main", "thumbnail1", "thumbnail2", "originalImage"];

    public function getServerpathAttribute() {
        return public_path("/modules/dynamicpagesrecords/images/".$this->record->id."/".$this->name);
    }

    public function getPublicpathAttribute() {
        return url("/modules/dynamicpagesrecords/images/".$this->record->id."/".$this->name);
    }

    public function getThumbnail1Attribute() {
        $thumbnail1 = new RecordImage([
            "original_name" => $this->name,
            "name" => "thumbnail1/".$this->name,
            "record_id" => $this->record_id
        ]);;

        // If thumbnail exists, return fake thumbnail model, otherwise return this photo
        if (file_exists($thumbnail1->serverpath))
            return $thumbnail1;
        return $this;
    }

    public function getThumbnail2Attribute() {
        $thumbnail2 = new RecordImage([
            "original_name" => $this->name,
            "name" => "thumbnail2/".$this->name,
            "record_id" => $this->record_id
        ]);

        // If thumbnail exists, return fake thumbnail model, otherwise return this photo
        if (file_exists($thumbnail2->serverpath))
            return $thumbnail2;
        return $this;
    }

    public function getOriginalImageAttribute() {
        $original = new RecordImage([
            "original_name" => $this->name,
            "name" => "original/".$this->name,
            "record_id" => $this->record_id
        ]);

        // If original exists, return fake thumbnail model, otherwise return this photo
        if (file_exists($original->serverpath))
            return $original;
        return $this;
    }

    /* RELATIONSHIPS */
    public function record() {
        return $this->belongsTo("\Modules\DynamicPages\Entities\Record");
    }



    /* METHODS */
    public function scopeOrdered($query) {
        return $query->orderBy('position', 'asc')->get();
    }

    public function scopeRelationshipOrdered($query) {
        return $query->orderBy('position', 'asc');
    }
    
    public function remove() {
    	// Delete physical file if it exists
        if (file_exists($this->serverpath))
            unlink($this->serverpath);

        // If this instance is not a thumbnail, delete its thumbnails
        if (substr($this->name, 0, 11) != "thumbnail1/" && substr($this->name, 0, 11) != "thumbnail2/") {
        	// If thumbnails actually exist (not just model copy), remove them
        	if ($this->thumbnail1 != $this)
        		$this->thumbnail1->remove();

            if ($this->thumbnail2 != $this)
        		$this->thumbnail2->remove();

            // Delete file from DB
            $this->delete();
        }
    }
}
