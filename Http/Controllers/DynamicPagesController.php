<?php

namespace Modules\DynamicPages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Modules\DynamicPages\Entities\Record;
use Modules\DynamicPages\Entities\RecordValue;
use Modules\DynamicPages\Entities\Category;
use Modules\DynamicPages\Entities\Url;
use Modules\DynamicPages\Entities\UrlTranslation;
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url as SitemapUrl;

class DynamicPagesController extends Controller {
    // Keep just one URL for every record/category, delete the rest
    public function cleanOldURLs() {
        if (!env("ALLOW_DP_URL_DB_CLEAN", false))
            abort(404);

        $urls = Url::where("id", "!=", false)->ordered();

        $keep = [];
        $remove = [];

        // Remove duplicit URLs
        foreach ($urls as $url) {
            $key = ($url->category_id ? $url->category_id : "n")."-".($url->record_id ? $url->record_id : "n");

            if (!isset($keep[$key]))
                $keep[$key] = 1;
            else
                $remove[] = $url->id;
        }

        \DB::table('dynamicpages__urls')->whereIn('id', $remove)->delete();
        \DB::table('dynamicpages__url_translations')->whereIn('url_id', $remove)->delete();

        return count($remove)." URL smaz�no, ".count($keep)." zachov�no.";
    }

    // Publish all required assets and create all required folders
    public static function publishAssets() {
        // Delete old assets folder
        if (file_exists(public_path("modules/dynamicpagesassets")))
            \File::deleteDirectory(public_path("modules/dynamicpagesassets"));

        /* --- CREATE FOLDERS --- */
        // Create DynamicPages folder
        if (!file_exists(public_path("/modules/dynamicpages"))) {
            mkdir(public_path("/modules/dynamicpages"));
            mkdir(public_path("/modules/dynamicpages/deleted"));
        }

        // Create DynamicPages/Assets folder
        if (!file_exists(public_path("modules/dynamicpages/assets")))
            mkdir(public_path("modules/dynamicpages/assets"));

        // Create DynamicPagesRecords folder
        if (!file_exists(public_path("modules/dynamicpagesrecords"))) {
            mkdir(public_path("modules/dynamicpagesrecords"));
            mkdir(public_path("modules/dynamicpagesrecords/images"));
            mkdir(public_path("modules/dynamicpagesrecords/files"));
            mkdir(public_path("modules/dynamicpagesrecords/main_images"));
        }


        // Move assets
        foreach(glob(__DIR__."/../../Assets/*") as $file) {
            if (!file_exists(public_path("modules/dynamicpages/assets/".basename($file))) && (!is_dir($file) || basename($file) != "_ckplugins")) {
                if(is_dir($file))
                    self::moveAssetsFolder(__DIR__."/../../Assets/".(basename($file) == "fileuploader" && file_exists(__DIR__."/../../Assets/fileuploader/dist") ? "fileuploader/dist" : basename($file)), public_path("modules/dynamicpages/assets/".basename($file)));
                elseif (basename($file) != ".gitkeep")
                    copy($file, public_path("modules/dynamicpages/assets/".basename($file)));
            }
        }

        // Move CKEDITOR plugins
        foreach (glob(__DIR__."/../../Assets/_ckplugins/*", GLOB_ONLYDIR) as $plugin) {
            if (!is_dir($plugin) || basename($plugin) == "_ckplugins")
                continue;

            if (file_exists(public_path("themes/adminlte/js/vendor/ckeditor/"))) {
                if (!file_exists(public_path("themes/adminlte/js/vendor/ckeditor/plugins/")))
                    mkdir(public_path("themes/adminlte/js/vendor/ckeditor/plugins/"));

                if (!file_exists(public_path("themes/adminlte/js/vendor/ckeditor/plugins/".basename($plugin))))
                    self::moveAssetsFolder($plugin, public_path("themes/adminlte/js/vendor/ckeditor/plugins/".basename($plugin)));
            }

            if (file_exists(public_path("themes/adminlte/assets/js/vendor/ckeditor/"))) {
                if (!file_exists(public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/")))
                    mkdir(public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/"));

                if (!file_exists(public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/".basename($plugin))))
                    self::moveAssetsFolder($plugin, public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/".basename($plugin)));
            }
        }

        // CKEDITOR files
        foreach (glob(__DIR__."/../../Assets/_ckplugins/*") as $file) {
            if (!is_file($file))
                continue;

            if (file_exists(public_path("themes/adminlte/js/vendor/ckeditor/"))) {
                if (!file_exists(public_path("themes/adminlte/js/vendor/ckeditor/plugins/")))
                    mkdir(public_path("themes/adminlte/js/vendor/ckeditor/plugins/"));

                \File::copy($file, public_path("themes/adminlte/js/vendor/ckeditor/plugins/".basename($file)));
            }

            if (file_exists(public_path("themes/adminlte/assets/js/vendor/ckeditor/"))) {
                if (!file_exists(public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/")))
                    mkdir(public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/"));

                \File::copy($file, public_path("themes/adminlte/assets/js/vendor/ckeditor/plugins/".basename($file)));
            }
        }
    }

    // Copy whole folder
    private static function moveAssetsFolder($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    self::moveAssetsFolder($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Generate sitemap from URLs from DB
     */
    public function generateSitemap() {
        $sitemap = Sitemap::create();

        $mainURLs = \DB::select("
            SELECT t.id, t.updated_at, dynamicpages__url_translations.url
            FROM dynamicpages__urls t
            JOIN `dynamicpages__url_translations` ON t.id = dynamicpages__url_translations.url_id
            WHERE (
                t.id = (select max(t2.id) from dynamicpages__urls t2 where t2.category_id = t.category_id)
                    OR 
                t.id = (select max(t2.id) from dynamicpages__urls t2 where t2.record_id = t.record_id)
            )
            ORDER BY t.id DESC
        ");

        $mainURLs = Url::with("translations")->hydrate($mainURLs);

        foreach ($mainURLs as $mainURL) {
            $x = 0;
            foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
                $link = $mainURL->translate($locale)->url;
                if ($link != "") {
                    $link = url($link);
                    if ($x == 0)
                        $url = SitemapUrl::create($link);
                    else
                        $url->addAlternate($link, $locale);
                    $x++;
                }
            }

            if (isset($url) && $url) {
                $url = $url
                    ->setLastModificationDate($mainURL->updated_at)
                    ->setChangeFrequency(SitemapUrl::CHANGE_FREQUENCY_WEEKLY);
                $sitemap->add($url);
            }
        }

        $filename = "sitemap.xml";
        $sitemap->writeToFile($filename);
        return "Sitemap '$filename' created with ".count($mainURLs)." URLs";
    }

    /**
     * Get item by requested URL
     * @param string    $url
     * @return mixed
     */
    public static function getItemFromURL($getUrl) {
        $getUrl = trim($getUrl);
        if ($getUrl == "")
            return null;

        $langExtend = "";
        if (mb_substr($getUrl, 0, 3) == locale()."/") {
            $langExtend = " AND dynamicpages__url_translations.locale = '".mb_substr($getUrl, 0, 2)."'";
            $getUrl = mb_substr($getUrl, 3);
        }

        // Test if any "active" URL is same as requested URL
        $currentUrl = \DB::select("
            SELECT t.category_id, t.record_id, dynamicpages__url_translations.url
            FROM dynamicpages__urls t
            JOIN `dynamicpages__url_translations` ON t.id = dynamicpages__url_translations.url_id
            WHERE (
                t.id = (select max(t2.id) from dynamicpages__urls t2 where t2.category_id = t.category_id)
                    OR 
                t.id = (select max(t2.id) from dynamicpages__urls t2 where t2.record_id = t.record_id)
            )
            AND
                dynamicpages__url_translations.url = '".$getUrl."' $langExtend
            ORDER BY t.id DESC
        ");

        // Current URL found
        if (count($currentUrl)) {
            $url = (object)$currentUrl[0];
            if ($url->record_id == null) {
                $category = Category::find($url->category_id);
                if ($category)
                    return $category;
            } else {
                $record = Record::find($url->record_id);
                if ($record)
                    return $record;
            }
        }

        // Test old URLs
        $url  = UrlTranslation::where("url", $getUrl)->orderByDesc("id");

        // Nothing found
        if (!$url->count())
            return null;

        $url = Url::findOrFail($url->first()->url_id);

        if ($url->record_id == null)
            return $url->category;
        return $url->record;
    }

    // Search entities
    public static function search($query) {
        $query = trim($query);
        if ($query == "")
            return false;

        $query = mb_strtolower($query);

        $usedCategories = [];
        $usedRecords = [];

        // Categories without content types "link" and "empty" - check only name and link for match
        $categories = Category::whereNotIn("content_type", ["link", "empty"])->get()->filter(function($item) use ($query, &$usedCategories, &$usedRecords) {
            if (strpos(mb_strtolower($item->name), $query) !== false || strpos(mb_strtolower($item->url), $query) !== false) {
                $usedCategories[] = $item->id;
                return true;
            }
            return false;
        });

        // Categories with content type "link" - add only if linked category is not already added
        Category::where("content_type", "link")->get()->filter(function($item) use ($query, $categories, &$usedCategories) {
            if ((strpos(mb_strtolower($item->name), $query) !== false || strpos(mb_strtolower($item->url), $query) !== false)
                && !in_array($item->content_id, $usedCategories)) {
                $categories->push($item);
                $usedCategories[] = $item->id;
            }
        });

        $records = Record::allVisible()->filter(function($item) use ($query, $categories, &$usedRecords, &$usedCategories) {
            $matches = strpos(mb_strtolower($item->name), $query) !== false || strpos(mb_strtolower($item->url), $query) !== false || strpos(mb_strtolower($item->meta_description), $query) !== false;

            // Slave of category and not used yet
            if ($matches) {
                // Slave but category is not used yet
                if ($item->slavingCategory && !in_array($item->slavingCategory->id, $usedCategories)) {
                    $categories->push($item->slavingCategory);
                    $usedCategories[] = $item->slavingCategory->id;
                    $usedRecords[] = $item->id;
                    return false;
                } elseif (!$item->slavingCategory) { // Not a slave
                    $usedRecords[] = $item->id;
                    return true;
                }
            }
            return false;
        });

        $recordValues = RecordValue::all()->filter(function($item) use ($query, $records, $categories, &$usedRecords, &$usedCategories) {
            // Matches and not used yet
            if (strpos(mb_strtolower($item->value), $query) !== false && !in_array($item->record_id, $usedRecords)) {

                // Record is visible
                if ($item->record && $item->record->visible == 1 && Carbon::now()->gte($item->record->published_since)) {
                    $usedRecords[] = $item->record_id;

                    // Record is slave
                    if ($item->record->slavingCategory) {
                        // Not used yet
                        if (!in_array($item->record->slavingCategory->id, $usedCategories)) {
                            $usedCategories[] = $item->record->slavingCategory->id;
                            $categories->push($item->record->slavingCategory);
                        }
                    } else
                        $records->push($item->record);
                }
            }
            return false;
        });

        return (object)[
            "records" => $records,
            "categories" => $categories
        ];
    }

    // Clear HTML cache
	public static function clearHTMLCache() {
    	if (array_has(Artisan::all(), "page-cache:clear"))
    		Artisan::call("page-cache:clear");
	}











    public function index() { return view('dynamicpages::index'); }
    public function create() { return view('dynamicpages::create'); }
    public function store(Request $request) { }
    public function show() { return view('dynamicpages::show'); }
    public function edit() { return view('dynamicpages::edit'); }
    public function update(Request $request) { }
    public function destroy() { }
}