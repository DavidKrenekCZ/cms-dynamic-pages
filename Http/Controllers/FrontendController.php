<?php

namespace Modules\DynamicPages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class FrontendController extends AdminBaseController {
	/**
	 * Fallback route - find DP entity and call method from configuration
	 *
	 * @param Request
	 * @return Response
	 */
    public function url(Request $request) {
        $item = DynamicPagesController::getItemFromURL(\Request::decodedPath());

        if (!$item)
            abort(404);

        return call_user_func(config("asgard.dynamicpages.core.frontend_route_method"), $item, $request);
    }
}
