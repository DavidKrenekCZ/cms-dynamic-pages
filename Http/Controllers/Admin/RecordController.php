<?php

namespace Modules\DynamicPages\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DynamicPages\Entities\Record;
use Modules\DynamicPages\Entities\Template;
use Modules\DynamicPages\Entities\Section;
use Modules\DynamicPages\Entities\RecordValue;
use Modules\DynamicPages\Entities\RecordImage;
use Modules\DynamicPages\Entities\RecordFile;
use Modules\DynamicPages\Entities\RecordTranslation;
use Modules\DynamicPages\Http\Controllers\DynamicPagesController;
use Modules\DynamicPages\Http\Requests\CreateRecordRequest;
use Modules\DynamicPages\Http\Requests\UpdateRecordRequest;
use Modules\DynamicPages\Repositories\RecordRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\DynamicPages\Http\Controllers\Admin\FileUploader;
use Carbon\Carbon;
use Modules\DynamicPages\Entities\Url;
use Intervention\Image\ImageManagerStatic as Image;

class RecordController extends AdminBaseController {
    /**
     * @var RecordRepository
     */
    private $record;

    public function __construct(RecordRepository $record) {
        parent::__construct();

        $this->record = $record;

        DynamicPagesController::publishAssets();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        $records = $this->record->all();

        return view('dynamicpages::admin.records.index', [
            "records" => $records,
            "templateFilter" => isset($request->template) ? $request->template : false,
            "goToRecord" => isset($request->goToRecord) && (int)$request->goToRecord > 0 ? (int)$request->goToRecord : false
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('dynamicpages::admin.records.create', [
            "record" => null
        ]);
    }

    public function cloneRecord(Record $record) {
        return view('dynamicpages::admin.records.create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRecordRequest $request
     * @return Response
     */
    public function store(CreateRecordRequest $request) {
        $data = $this->getRecordData($request);

        if (is_string($data))
            return redirect()->back()->withInput($request->all())->withError(trans("dynamicpages::categories.validation.".$data));

        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
            $rt = RecordTranslation::where("url", $data[$locale]["url"]);
            if ($rt->count())
                foreach ($rt->get() as $r)
                    if (Record::find($r->record_id))
                        return redirect()->back()->withInput($data)->withError(trans("dynamicpages::records.validation.url", ["lang" => $language["native"]]));
        }

        $record = $this->record->create($data);
        Url::composeUrl($record);

        foreach ($this->getRecordValuesData($request, $record) as $value) {
            $rv = new RecordValue($value);
            $rv->save();
        }

        $this->saveFiles($record, $request);

        DynamicPagesController::clearHTMLCache();

        return redirect()->route('admin.dynamicpages.record.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('dynamicpages::records.title.records')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Record $record
     * @return Response
     */
    public function edit(Record $record) {
        return view('dynamicpages::admin.records.edit', compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Record $record
     * @param  UpdateRecordRequest $request
     * @return Response
     */
    public function update(Record $record, UpdateRecordRequest $request) {
        $inputs = $this->getRecordData($request, $record);

        if (is_string($inputs))
            return redirect()->back()->withInput($request->all())->withError(trans("dynamicpages::categories.validation.".$inputs));

        $this->record->update($record, $inputs);
        Url::composeUrl($record);

        foreach ($this->getRecordValuesData($request, $record) as $fieldId => $value) {
            $rv = RecordValue::where([["template_field_id", $fieldId], ["record_id", $record->id]])->first();
            if ($rv && $rv->count())
                $rv->update($value);
            else {
                $rv = new RecordValue($value);
                $rv->save();
            }
        }

        $this->saveFiles($record, $request);

        DynamicPagesController::clearHTMLCache();

        if (isset($request->updateAndStay))
            return redirect()->route('admin.dynamicpages.record.edit', ["record" => $record])
                ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('dynamicpages::records.title.records')]));

        return redirect()->route('admin.dynamicpages.record.index', ['goToRecord' => $record->id])
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('dynamicpages::records.title.records')]));
    }

    private function getRecordData($request, $record=false) {
        $recordData = $request->all();

        // Visibility
        if (!$record)
            $recordData["visible"] = isset($recordData["save-visible"]) ? 1 : 0;
        else
            $recordData["visible"] = $record->visible;

        // Name
        $defaultLocale = config("asgard.dynamicpages.core.default_locale");
        if (!$defaultLocale)
            $defaultLocale = "cs";

        $recordData[$defaultLocale]["name"] = isset($recordData[$defaultLocale]["name"]) ? trim($recordData[$defaultLocale]["name"]) : "";
        if ($recordData[$defaultLocale]["name"] == "")
            return "empty name";

        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
            if ($locale != $defaultLocale) {
                $recordData[$locale]["name"] = isset($recordData[$locale]["name"]) ? trim($recordData[$locale]["name"]) : "";

                if ($recordData[$locale]["name"] == "")
                    $recordData[$locale]["name"] = " ";
            }
        }

        // Safe slug - if slug not set, generate unique one
        $template = Template::findOrFail($recordData["template_id"]);
        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language)
            if (trim($recordData[$locale]["url"]) == "" || !$template->settings_url) {
                $x = 0;
                do {
                    if ($locale != $defaultLocale && trim($recordData[$defaultLocale]["url"]) != "")
                        $slug = str_slug($recordData[$defaultLocale]["url"].($x != 0 ? "-".$x : ""));
                    elseif (trim($recordData[$locale]["url"]) == "")
                        $slug = str_random(25);
                    else
                        $slug = str_slug($recordData[$locale]["name"].($x != 0 ? "-".$x : ""));
                    $x++;
                } while(RecordTranslation::where([
                    ["url", $slug],
                    ["locale", $locale]
                ])->count());

                $recordData[$locale]["url"] = $slug;
            }

        // Sections
        $sections = [];
        if (isset($recordData["sections"]))
            foreach ($recordData["sections"] as $sectionId => $value)
                if ($value == 1 && Section::find($sectionId)->template_id == $recordData["template_id"])
                    $sections[] = $sectionId;
        $recordData["sections_array"] = json_encode($sections);

        // Categories
        if (isset($recordData["categories"]))
            $recordData["categories_array"] = json_encode($recordData["categories"]);
        else
            $recordData["categories_array"] = json_encode([]);

        // Published since
        $ps = trim($recordData["published_since"]);
        if ($ps != "" && $ps = Carbon::createFromFormat("d. m. Y H:i", $ps))
            $recordData["published_since"] = $ps->timestamp;
        else
            $recordData["published_since"] = time();

        if ((!isset($recordData["no_published_to"]) || !$recordData["no_published_to"]) && $recordData["published_to"] != "" && $pt = Carbon::createFromFormat("d. m. Y H:i", $recordData["published_to"]))
            $recordData["published_to"] = $pt->timestamp;
        else
            $recordData["published_to"] = null;

        return $recordData;
    }

    private function getRecordValuesData($request, $record) {
        $recordData = $request->all();
        $recordValues = [];
        $recordId = $record->id;
        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
            $lang = $recordData[$locale];

            $record->load("template.fields");
            // Inputs values
            foreach ($lang as $input => $value) {
                $split = explode("_", $input);
                if (count($split) == 3 && $split[0] == "templatefields") {
                    $templateId = $split[1];
                    if ($templateId == $recordData["template_id"]) {
                        $fieldId = $split[2];
                        $field = $record->template->fields->find($fieldId);

                        // Value not set yet - init basic variables
                        if (!isset($recordValues[$fieldId]))
                            $recordValues[$fieldId] = [
                                "record_id" => $recordId,
                                "template_field_id" => $fieldId
                            ];

                        // Locale value not set yet
                        if (!isset($recordValues[$fieldId][$locale]))
                            $recordValues[$fieldId][$locale] = [];

                        if (trim($value) == "")
                            $value = " ";

                        // Parse record IDs from autocomplete and create new string of IDs
                        if ($field->type == "ac-tags" && $value != " ") {
                            $originalValue = $value;
                            $value = [];

                            $records = explode(",", $originalValue);
                            foreach ($records as $r)
                                if (preg_match_all("/^#([0-9]+) - .+$/", $r, $match))
                                    $value[] = $match[1][0];

                            $value = implode(",", $value);
                        }

                        $recordValues[$fieldId][$locale]["value"] = $value;
                    }
                }
            }

            // Checkbox values
            foreach ($record->template->fields->where("type", "checkbox") as $field) {
                if (!isset($recordValues[$field->id]))
                    $recordValues[$field->id] = [
                        "record_id" => $recordId,
                        "template_field_id" => $field->id
                    ];

                if (!isset($recordValues[$field->id][$locale]))
                    $recordValues[$field->id][$locale] = [];
                $recordValues[$field->id][$locale]["value"] = isset($recordData[$locale][$field->formName]) ? "1" : "0";
            }
        }
        return $recordValues;
    }

    private function saveFiles(Record $record, Request $request) {
        // Create files folder if doesn't exist yet
        if (!file_exists($record->filesDir))
            mkdir($record->filesDir, 0700, 1);

        // Create images folder if doesn't exist yet
        if (!file_exists($record->imagesDir))
            mkdir($record->imagesDir, 0700, 1);

        // Delete main image if user wanted to
        if (isset($request->remove_main_image) && $request->remove_main_image == "1" && $record->serverMainImage && file_exists($record->serverMainimage))
            unlink($record->serverMainImage);


        if ($request->files->has("main_image")) {
            $file = $request->files->get("main_image");

            $name = $file->getClientOriginalName();
            $explode = explode('.', $name);
            $extension = end($explode);

            $file->move(public_path("/modules/dynamicpagesrecords/main_images"), $record->id.".".$extension);
        }

        $this->handleFiles($record, $request, RecordFile::class, false);
        $this->handleFiles($record, $request, RecordImage::class, true);

        return true;
    }

    private function handleFiles($record, $request, $class, $images) {
        if (!$images)
            $appendedFiles = json_decode($record->appendedFiles, 1);
        else
            $appendedFiles = json_decode($record->appendedImages, 1);

        $FileUploader = new FileUploader($images ? "images" : "files", array(
            'uploadDir' => ($images ? $record->imagesDir : $record->filesDir)."/",
            'title' => 'name',
            'files' => $appendedFiles,
        ));

        foreach($FileUploader->getRemovedFiles('file') as $key=>$value) {
            $f = $class::where([["record_id", $record->id], ["name", $value["name"]]])->first();
            if ($f && $f->count())
                $f->remove();
        }

        $FileUploader->upload();

        if ($images)
            foreach (RecordImage::where("record_id", $record->id)->get() as $img)
                foreach (["thumbnail2", "thumbnail1"] as $type)
                    $this->resizeImage($record, $img->name, $type, true);

        foreach ($FileUploader->getFileList() as $f) {
            $file = $class::where("record_id", $record->id)->where("name", $f["name"])->first();

            if (!$file || !$file->count()) {
                $file = new $class([
                    "record_id" => $record->id,
                    "name" => $f["name"],
                    "position" => $f["index"]
                ]);
                $file->save();

                // Save original of photo before resizing it
                if ($images && config("asgard.dynamicpages.core.imgs.save_original")) {
                    $destination = $record->imagesDir."/original";
                    if (!file_exists($destination))
                        mkdir($destination);

                    $img = Image::make($record->imagesDir."/".$f["name"]);
                    $img->save($destination."/".$f["name"]);
                }

                if ($images && config("asgard.dynamicpages.core.imgs.resize")) {
                    foreach (["main", "thumbnail2", "thumbnail1"] as $type)
                        $this->resizeImage($record, $f["name"], $type);
                }
            } else
                $file->update(["position" => $f["index"]]);
        }
    }

    private function resizeImage($record, $name, $type, $assurance=false) {
        $folder = $type == "main" ? $record->imagesDir : $record->imagesDir."/".$type;
        if (!file_exists($folder))
            mkdir($folder);

        if (file_exists($folder."/".$name) && $assurance)
            return false;

        $height = config("asgard.dynamicpages.core.imgs.resize.attribute") == "height" ? config("asgard.dynamicpages.core.imgs.resize.".$type."_size") : null;
        $width = config("asgard.dynamicpages.core.imgs.resize.attribute") == "width" ? config("asgard.dynamicpages.core.imgs.resize.".$type."_size") : null;

        $img = Image::make($record->imagesDir."/".$name);
        $img->resize($width, $height, function ($constraint) { $constraint->aspectRatio(); $constraint->upsize(); });
        return $img->save($folder."/".$name);
    }


    /**
     * Crop or rotate image - AJAX request
     *
     * @param Record    $record
     * @param Request   $request
     * @return Response
     */
    public function imageEditor(Record $record, Request $request) {
        $name = $request->file;
        $content = $request->content;

        // Find image
        $image = RecordImage::where([
            [ "record_id", $record->id ],
            [ "name", $name ]
        ])->firstOrFail();

        // Delete image with all thumbnails etc
        \File::delete($image->serverpath, $image->originalImage->serverpath, $image->thumbnail1->serverpath, $image->thumbnail2->serverpath);

        // Save image (in original size)
        $img = Image::make($content);
        $img->save($record->imagesDir."/".$name);

        // Save original image, if configured
        if (config("asgard.dynamicpages.core.imgs.save_original")) {
            $destination = $record->imagesDir."/original";
            if (!file_exists($destination))
                mkdir($destination);

            $img = Image::make($record->imagesDir."/".$name);
            $img->save($destination."/".$name);
        }

        // Save resized variations
        if (config("asgard.dynamicpages.core.imgs.resize"))
            foreach (["main", "thumbnail2", "thumbnail1"] as $type)
                $this->resizeImage($record, $name, $type);

        return response()->json(["ok" => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Record $record
     * @return Response
     */
    public function destroy(Record $record) {
        $record->urls()->delete();
        $this->record->destroy($record);

        DynamicPagesController::clearHTMLCache();

        return redirect()->route('admin.dynamicpages.record.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('dynamicpages::records.title.records')]));
    }

    public function multiDelete(Request $request) {
        foreach ($request->ids as $id) {
            $record = Record::find($id);
            $record->delete();
        }
        DynamicPagesController::clearHTMLCache();
        return "";
    }

    public function toggleVisibility(Record $record) {
        $record->visible = !$record->visible;
        $record->save();
        DynamicPagesController::clearHTMLCache();
        return redirect()->route('admin.dynamicpages.record.index')
            ->withSuccess(trans('dynamicpages::records.messages.visibility toggled'));
    }

    public function downloadFile($fileId) {
        $file = RecordFile::findOrFail($fileId);
        return response()->download($file->serverpath, $file->name);
    }

    public function showFile($fileId) {
        $file = RecordFile::findOrFail($fileId);
        return response()->file($file->serverpath);
    }

    // Return records for autocomplete
    public function typeahead($categoryId=false) {
        if (!$categoryId || $categoryId == "all")
            $records = Record::all();
        else
            $records = Record::getByCategory($categoryId);

        return $records->map(function($item) {
            return [
                "id" => $item->autocompleteSlug,
                "name" => $item->name
            ];
        });
    }
}
