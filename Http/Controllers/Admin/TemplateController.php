<?php

namespace Modules\DynamicPages\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DynamicPages\Entities\Template;
use Modules\DynamicPages\Entities\TemplateField;
use Modules\DynamicPages\Http\Requests\CreateTemplateRequest;
use Modules\DynamicPages\Http\Requests\UpdateTemplateRequest;
use Modules\DynamicPages\Repositories\TemplateRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class TemplateController extends AdminBaseController {
    /**
     * @var TemplateRepository
     */
    private $template;

    public function __construct(TemplateRepository $template) {
        parent::__construct();

        $this->template = $template;

        \Modules\DynamicPages\Http\Controllers\DynamicPagesController::publishAssets();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $templates = $this->template->all();

        return view('dynamicpages::admin.templates.index', compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('dynamicpages::admin.templates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateTemplateRequest $request
     * @return Response
     */
    public function store(CreateTemplateRequest $request) {
        $template = new Template($request->all());
        $template->save();

        foreach ($request->field_type as $index => $value) {
            if ($index != 0) {
                $array = [
                    "template_id" => $template->id,
                    "position" => $index,
                    "system_name" => $request["field_system_name"][$index],
                    "type" => $value
                ];
                foreach (\LaravelLocalization::getSupportedLocales() as $locale => $lang)
                    $array[$locale] = ["name" => $request["field_name_".$locale][$index]];
                $field = new TemplateField($array);
                $field->save();
            }
        }

        return redirect()->route('admin.dynamicpages.template.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('dynamicpages::templates.title.templates')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Template $template
     * @return Response
     */
    public function edit(Template $template) {
        return view('dynamicpages::admin.templates.edit', compact('template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Template $template
     * @param  UpdateTemplateRequest $request
     * @return Response
     */
    public function update(Template $template, UpdateTemplateRequest $request) {
        $this->template->update($template, $request->all());

        // Remove deleted fields
        foreach ($template->fields as $field)
            if (!in_array($field->id, $request->field_id))
                $field->delete();

        // Update fields and create new
        foreach ($request->field_type as $index => $value) {
            if ($index != 0) {
                $array = [
                    "template_id" => $template->id,
                    "position" => $index,
                    "system_name" => $request["field_system_name"][$index],
                    "type" => $value
                ];

                foreach (\LaravelLocalization::getSupportedLocales() as $locale => $lang)
                    $array[$locale] = ["name" => $request["field_name_".$locale][$index]];

                $id = $request->field_id[$index];
                if ($id == "new_field") {
                    $field = new TemplateField($array);
                    $field->save();
                } else {
                    $field = TemplateField::findOrFail($id);
                    $field->update($array);
                }
            }
        }

        return redirect()->route('admin.dynamicpages.template.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('dynamicpages::templates.title.templates')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Template $template
     * @return Response
     */
    public function destroy(Template $template) {
        if (!$template->records->count()) {
            $this->template->destroy($template);
            return redirect()->route('admin.dynamicpages.template.index')
                ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('dynamicpages::templates.title.templates')]));
        }
        return redirect()->route('admin.dynamicpages.template.index')
            ->withError(trans('dynamicpages::templates.messages.template undeletable'));
    }
}
