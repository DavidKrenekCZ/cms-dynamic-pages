<?php

namespace Modules\DynamicPages\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DynamicPages\Entities\Category;
use Modules\DynamicPages\Http\Controllers\DynamicPagesController;
use Modules\DynamicPages\Http\Requests\CreateCategoryRequest;
use Modules\DynamicPages\Http\Requests\UpdateCategoryRequest;
use Modules\DynamicPages\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\DynamicPages\Entities\Url;

class CategoryController extends AdminBaseController
{
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(CategoryRepository $category)
    {
        parent::__construct();

        $this->category = $category;

        DynamicPagesController::publishAssets();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::structuredAll();

        return view('dynamicpages::admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dynamicpages::admin.categories.create', [
            "isAdmin" => \Auth::user()->hasRoleName("admin")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request) {
        $inputs = $this->getStoreData($request);

        if (is_string($inputs))
            return redirect()->back()->withInput($request->all())->withError(trans("dynamicpages::categories.validation.".$inputs));

        $category = $this->category->create($inputs);
        Url::composeUrl($category);

        DynamicPagesController::clearHTMLCache();

        return redirect()->route('admin.dynamicpages.category.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('dynamicpages::categories.title.categories')]));
    }

    private function uploadFile($file) {
        $name = $file->getClientOriginalName();
        $explode = explode('.', $name);
        $extension = end($explode);
        do {
            $filename = str_random(32).".".$extension;
        } while(file_exists(public_path("/modules/dynamicpages/$filename")) or file_exists(public_path("/modules/dynamicpages/deleted/$filename")));

        $file->move(public_path("/modules/dynamicpages"), $filename);
        return $filename;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        return view('dynamicpages::admin.categories.edit', [
            "isAdmin" => \Auth::user()->hasRoleName("admin"),
            "category" => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     * @param  UpdateCategoryRequest $request
     * @return Response
     */
    public function update(Category $category, UpdateCategoryRequest $request)
    {
        $inputs = $this->getStoreData($request, $category);

        if (is_string($inputs))
            return redirect()->back()->withInput($request->all())->withError(trans("dynamicpages::categories.validation.".$inputs));

        $this->category->update($category, $inputs);
        Url::composeUrl($category);

        DynamicPagesController::clearHTMLCache();

        return redirect()->route('admin.dynamicpages.category.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('dynamicpages::categories.title.categories')]));
    }

    private function getStoreData($request, $category=null) {
        $inputs = $request->all();
        if ((int)$inputs["parent_id"] == 0)
            $inputs["parent_id"] = null;

        /* if (!\Auth::user()->hasRoleName("admin") && ($inputs["content_type"] != "record" && $inputs["content_type"] != "list" && $inputs["content_type"] != "url") && !config("asgard.dynamicpages.core.dont_check_category_permissions"))
            return "content not admin"; */

        if ($request->files->has("icon_file") && $inputs["parent_id"] != null) {
            $inputs["icon"] = $this->uploadFile($request->files->get("icon_file"));

            if ($category && $category->hasIcon())
                rename(public_path("/modules/dynamicpages/".$category->icon), public_path("/modules/dynamicpages/deleted/".$category->icon));
        }

        if ($inputs["content_type"] == "record")
            $inputs["content_id"] = $inputs["content_id_record"];
        elseif ($inputs["content_type"] == "list" || $inputs["content_type"] == "subcategory")
            $inputs["content_id"] = $inputs["content_id_list"];
        elseif ($inputs["content_type"] == "link" || $inputs["content_type"] == "child-link")
            $inputs["content_id"] = $inputs["content_id_link"];
        else
            $inputs["content_id"] = null;

        // ---  Validation ---
        // Can't change parent type to URL if parent has children
        if ($inputs["parent_id"] == null && $inputs["content_type"] == "url" && $category && $category->children->count())
            return "parent url change";

        // Can't insert children to parent with type URL
        if ($inputs["parent_id"] != null && Category::findOrFail($inputs["parent_id"])->content_type == "url")
            return "parent url";

        // Can't change parent type to list if parent has children of type list
        if ($inputs["parent_id"] == null && $inputs["content_type"] == "list" && $category && $category->children->count() && $category->children->where("content_type", "list")->count())
            return "parent list change";

        // Can't insert children type list to parent with type list
        if ($inputs["parent_id"] != null && $inputs["content_type"] == "list" && Category::findOrFail($inputs["parent_id"])->content_type == "list")
            return "parent list";


        // Only parent categories can be type empty
        if ($inputs["parent_id"] != null && $inputs["content_type"] == "empty")
            return "empty subcategory";

        // Can't link to oneself
        if ($category && $inputs["content_type"] == "link" && $inputs["content_id_link"] == $category->id)
            return "invalid link";

        // Can't link to a child
        if ($category && $inputs["content_type"] == "link" && Category::findOrFail($inputs["content_id"])->parent_id == $category->id)
            return "link to child";

        // Child-link type must be a parent
        if ($inputs["parent_id"] != null && $inputs["content_type"] == "child-link")
            return "child-link subcategory";

        // Child-link must be linked to one's child
        if ($inputs["content_type"] == "child-link" && $category && Category::findOrFail($inputs["content_id"])->parent_id != $category->id)
            return "child-link not child";

        /*
        // Parent category can't be type link
        if ($inputs["parent_id"] == null && $inputs["content_type"] == "link")
            return "main category link";
        */

        // Default name can't be empty
        $defaultLocale = config("asgard.dynamicpages.core.default_locale");
        if (!$defaultLocale)
            $defaultLocale = "cs";

        $inputs[$defaultLocale]["name"] = isset($inputs[$defaultLocale]["name"]) ? trim($inputs[$defaultLocale]["name"]) : "";
        if ($inputs[$defaultLocale]["name"] == "")
            return "empty name";

        // Default URL can't be empty
        $inputs[$defaultLocale]["url"] = isset($inputs[$defaultLocale]["url"]) ? trim($inputs[$defaultLocale]["url"]) : "";
        if (($inputs["content_type"] == "list" || $inputs["content_type"] == "subcategory" || $inputs["content_type"] == "url") && $inputs[$defaultLocale]["url"] == "")
            return "empty url";

        foreach (\LaravelLocalization::getSupportedLocales() as $locale => $language) {
            if ($locale != $defaultLocale) {
                $inputs[$locale]["name"] = isset($inputs[$locale]["name"]) ? trim($inputs[$locale]["name"]) : "";

                if ($inputs[$locale]["name"] == "")
                    $inputs[$locale]["name"] = " ";

                $inputs[$locale]["url"] = isset($inputs[$locale]["url"]) ? trim($inputs[$locale]["url"]) : "";

                if (($inputs["content_type"] == "list" || $inputs["content_type"] == "subcategory" || $inputs["content_type"] == "url") && $inputs[$locale]["url"] == "")
                    $inputs[$locale]["url"] = " ";
            }
        }

        return $inputs;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function destroy(Category $category) {
        $category->urls()->delete();
        $this->category->destroy($category);
        if ($category->hasIcon())
            rename(public_path("/modules/dynamicpages/".$category->icon), public_path("/modules/dynamicpages/deleted/".$category->icon));

        DynamicPagesController::clearHTMLCache();

        return redirect()->route('admin.dynamicpages.category.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('dynamicpages::categories.title.categories')]));
    }

    public function changePosition(Request $request) {
        foreach ($request->positions as $id => $position) {
            $cat = Category::find($id);
            $cat->position = $position;
            $cat->save();
        }
        DynamicPagesController::clearHTMLCache();
        return $request->positions;
    }

    // Bug fix - delete categories whose parents were deleted
    public function deleteOrphans() {
        $x = 0;
        foreach (Category::onlyTrashed()->get() as $category)
            foreach ($category->children as $child) {
                $child->delete();
                $x++;
            }
        echo "Done. ".$x." orphans deleted.";
    }
}
