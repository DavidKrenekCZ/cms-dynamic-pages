<?php

namespace Modules\DynamicPages\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DynamicPages\Entities\Section;
use Modules\DynamicPages\Entities\SectionField;
use Modules\DynamicPages\Http\Requests\CreateSectionRequest;
use Modules\DynamicPages\Http\Requests\UpdateSectionRequest;
use Modules\DynamicPages\Repositories\SectionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class SectionController extends AdminBaseController {
    /**
     * @var SectionRepository
     */
    private $section;

    public function __construct(SectionRepository $section) {
        parent::__construct();

        $this->section = $section;

        \Modules\DynamicPages\Http\Controllers\DynamicPagesController::publishAssets();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSectionRequest $request
     * @return Response
     */
    public function store(CreateSectionRequest $request) {
        $section = new Section($request->all());
        $section->save();

        foreach ($request->all()["sectionfields_".$request->template_id] as $fieldId => $status) {
            $array = [
                "template_field_id" => $fieldId,
                "section_id" => $section->id,
                "show" => $status
            ];
            $field = new SectionField($array);
            $field->save();
        }

        return redirect()->route('admin.dynamicpages.template.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('dynamicpages::sections.title.sections')]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Section $section
     * @param  UpdateSectionRequest $request
     * @return Response
     */
    public function update(UpdateSectionRequest $request) {
        $section = Section::findOrFail($request->section_edit_id);
        $this->section->update($section, $request->all());

        foreach ($request->all()["sectionfields_".$request->template_id] as $fieldId => $status) {
            $field = SectionField::where([["template_field_id", $fieldId], ["section_id", $section->id]])->first();
            $field->update([ "show" => $status ]);
        }

        return redirect()->route('admin.dynamicpages.template.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('dynamicpages::sections.title.sections')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Section $section
     * @return Response
     */
    public function destroy(Section $section) {
        $this->section->destroy($section);

        return redirect()->route('admin.dynamicpages.template.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('dynamicpages::sections.title.sections')]));
    }
}
