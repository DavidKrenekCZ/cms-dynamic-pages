<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/dynamicpages'], function (Router $router) {
    $router->get("records/download-file/{file}", [
        "as" => "admin.dynamicpages.download-file",
        "uses" => "Admin\RecordController@downloadFile"
    ]);
    $router->get("records/show-file/{file}", [
        "as" => "admin.dynamicpages.show-file",
        "uses" => "Admin\RecordController@showFile"
    ]);
    $router->get("clean-old-db-urls", [
        "uses" => "DynamicPagesController@cleanOldURLs"
    ]);
    $router->get("generate-sitemap", [
    	"as" => "admin.dynamicpages.generate-sitemap",
    	"uses" => "DynamicPagesController@generateSitemap"
    ]);
});

$frontendRouteMethod = config("asgard.dynamicpages.core.frontend_route_method");
if ($frontendRouteMethod) {
    $settings = [
        "middleware" => config("asgard.page.config.middleware"),
        "uses"          => "FrontendController@url"
    ];

    if (strpos($frontendRouteMethod, "@"))
        $settings["uses"] = $frontendRouteMethod;

    $router->get("{url}", $settings)->where("url", ".*");
}
