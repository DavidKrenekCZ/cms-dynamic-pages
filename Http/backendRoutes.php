<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/dynamicpages'], function (Router $router) {
    $router->bind('template', function ($id) {
        return app('Modules\DynamicPages\Repositories\TemplateRepository')->find($id);
    });
    $router->get('templates', [
        'as' => 'admin.dynamicpages.template.index',
        'uses' => 'TemplateController@index',
        'middleware' => 'can:dynamicpages.templates.index'
    ]);
    $router->get('templates/create', [
        'as' => 'admin.dynamicpages.template.create',
        'uses' => 'TemplateController@create',
        'middleware' => 'can:dynamicpages.templates.create'
    ]);
    $router->post('templates', [
        'as' => 'admin.dynamicpages.template.store',
        'uses' => 'TemplateController@store',
        'middleware' => 'can:dynamicpages.templates.create'
    ]);
    $router->get('templates/{template}/edit', [
        'as' => 'admin.dynamicpages.template.edit',
        'uses' => 'TemplateController@edit',
        'middleware' => 'can:dynamicpages.templates.edit'
    ]);
    $router->put('templates/{template}', [
        'as' => 'admin.dynamicpages.template.update',
        'uses' => 'TemplateController@update',
        'middleware' => 'can:dynamicpages.templates.edit'
    ]);
    $router->delete('templates/{template}', [
        'as' => 'admin.dynamicpages.template.destroy',
        'uses' => 'TemplateController@destroy',
        'middleware' => 'can:dynamicpages.templates.destroy'
    ]);

    $router->bind('section', function ($id) {
        return app('Modules\DynamicPages\Repositories\SectionRepository')->find($id);
    });
    $router->post('sections', [
        'as' => 'admin.dynamicpages.section.store',
        'uses' => 'SectionController@store',
        'middleware' => 'can:dynamicpages.templates.index'
    ]);
    $router->post('sections/edit', [
        'as' => 'admin.dynamicpages.section.update',
        'uses' => 'SectionController@update',
        'middleware' => 'can:dynamicpages.templates.index'
    ]);
    $router->delete('sections/{section}', [
        'as' => 'admin.dynamicpages.section.destroy',
        'uses' => 'SectionController@destroy',
        'middleware' => 'can:dynamicpages.templates.index'
    ]);

    $router->bind('record', function ($id) {
        return app('Modules\DynamicPages\Repositories\RecordRepository')->find($id);
    });
    $router->get('records', [
        'as' => 'admin.dynamicpages.record.index',
        'uses' => 'RecordController@index',
        'middleware' => 'can:dynamicpages.records.index'
    ]);
    $router->get('records/create', [
        'as' => 'admin.dynamicpages.record.create',
        'uses' => 'RecordController@create',
        'middleware' => 'can:dynamicpages.records.create'
    ]);
    $router->post('records', [
        'as' => 'admin.dynamicpages.record.store',
        'uses' => 'RecordController@store',
        'middleware' => 'can:dynamicpages.records.create'
    ]);
    $router->get('records/{record}/edit', [
        'as' => 'admin.dynamicpages.record.edit',
        'uses' => 'RecordController@edit',
        'middleware' => 'can:dynamicpages.records.edit'
    ]);
    $router->get('records/{record}/clone', [
        'as' => 'admin.dynamicpages.record.clone',
        'uses' => 'RecordController@cloneRecord',
        'middleware' => 'can:dynamicpages.records.create'
    ]);
    $router->put('records/{record}', [
        'as' => 'admin.dynamicpages.record.update',
        'uses' => 'RecordController@update',
        'middleware' => 'can:dynamicpages.records.edit'
    ]);
    $router->delete('records/{record}', [
        'as' => 'admin.dynamicpages.record.destroy',
        'uses' => 'RecordController@destroy',
        'middleware' => 'can:dynamicpages.records.destroy'
    ]);
    $router->post("records/multi", [
        "as" => "admin.dynamicpages.records.multidelete",
        "uses" => "RecordController@multiDelete",
        //"middleware" => "can:dynamicpages.records.destroy"
    ]);
    $router->get('records/{record}/toggle-visibility', [
        'as' => 'admin.dynamicpages.record.toggle-visibility',
        'uses' => 'RecordController@toggleVisibility',
        'middleware' => 'can:dynamicpages.records.edit'
    ]);

    $router->post('records/{record}/image-editor', [
        'as' => 'admin.dynamicpages.record.image-editor',
        'uses' => 'RecordController@imageEditor',
        'middleware' => 'can:dynamicpages.records.edit'
    ]);

    $router->get("typeahead/{categoryId}", [
        "as" => "admin.dynamicpages.record.typeahead",
        "uses" => "RecordController@typeahead"
    ]);


    $router->bind('category', function ($id) {
        return app('Modules\DynamicPages\Repositories\CategoryRepository')->find($id);
    });
    $router->get('categories', [
        'as' => 'admin.dynamicpages.category.index',
        'uses' => 'CategoryController@index',
        'middleware' => 'can:dynamicpages.categories.index'
    ]);
    $router->get('categories/create', [
        'as' => 'admin.dynamicpages.category.create',
        'uses' => 'CategoryController@create',
        'middleware' => 'can:dynamicpages.categories.create'
    ]);
    $router->post('categories', [
        'as' => 'admin.dynamicpages.category.store',
        'uses' => 'CategoryController@store',
        'middleware' => 'can:dynamicpages.categories.create'
    ]);
    $router->get('categories/{category}/edit', [
        'as' => 'admin.dynamicpages.category.edit',
        'uses' => 'CategoryController@edit',
        'middleware' => 'can:dynamicpages.categories.edit'
    ]);
    $router->put('categories/{category}', [
        'as' => 'admin.dynamicpages.category.update',
        'uses' => 'CategoryController@update',
        'middleware' => 'can:dynamicpages.categories.edit'
    ]);
    $router->delete('categories/{category}', [
        'as' => 'admin.dynamicpages.category.destroy',
        'uses' => 'CategoryController@destroy',
        'middleware' => 'can:dynamicpages.categories.destroy'
    ]);
    $router->post("categories/change-position", [
        "as" => "admin.dynamicpages.category.change-position",
        "uses" => "CategoryController@changePosition",
        "middleware" => 'can:dynamicpages.categories.index'
    ]);
    $router->get("categories/delete-orphans", [
        "middleware" => 'can:dynamicpages.categories.destroy',
        "uses" => "CategoryController@deleteOrphans"
    ]);
});
