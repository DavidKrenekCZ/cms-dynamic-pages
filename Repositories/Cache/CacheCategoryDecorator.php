<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\CategoryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCategoryDecorator extends BaseCacheDecorator implements CategoryRepository
{
    public function __construct(CategoryRepository $category)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.categories';
        $this->repository = $category;
    }
}
