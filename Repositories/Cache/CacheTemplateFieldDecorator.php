<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\TemplateFieldRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTemplateFieldDecorator extends BaseCacheDecorator implements TemplateFieldRepository
{
    public function __construct(TemplateFieldRepository $templatefield)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.templatefields';
        $this->repository = $templatefield;
    }
}
