<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\CategoryGroupRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCategoryGroupDecorator extends BaseCacheDecorator implements CategoryGroupRepository
{
    public function __construct(CategoryGroupRepository $categorygroup)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.categorygroups';
        $this->repository = $categorygroup;
    }
}
