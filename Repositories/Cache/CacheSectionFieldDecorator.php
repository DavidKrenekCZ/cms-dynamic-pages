<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\SectionFieldRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSectionFieldDecorator extends BaseCacheDecorator implements SectionFieldRepository
{
    public function __construct(SectionFieldRepository $sectionfield)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.sectionfields';
        $this->repository = $sectionfield;
    }
}
