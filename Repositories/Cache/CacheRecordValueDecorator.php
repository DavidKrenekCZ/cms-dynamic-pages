<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\RecordValueRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRecordValueDecorator extends BaseCacheDecorator implements RecordValueRepository
{
    public function __construct(RecordValueRepository $recordvalue)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.recordvalues';
        $this->repository = $recordvalue;
    }
}
