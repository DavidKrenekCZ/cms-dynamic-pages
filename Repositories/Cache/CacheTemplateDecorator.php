<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\TemplateRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTemplateDecorator extends BaseCacheDecorator implements TemplateRepository
{
    public function __construct(TemplateRepository $template)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.templates';
        $this->repository = $template;
    }
}
