<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\SectionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSectionDecorator extends BaseCacheDecorator implements SectionRepository
{
    public function __construct(SectionRepository $section)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.sections';
        $this->repository = $section;
    }
}
