<?php

namespace Modules\DynamicPages\Repositories\Cache;

use Modules\DynamicPages\Repositories\RecordRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRecordDecorator extends BaseCacheDecorator implements RecordRepository
{
    public function __construct(RecordRepository $record)
    {
        parent::__construct();
        $this->entityName = 'dynamicpages.records';
        $this->repository = $record;
    }
}
