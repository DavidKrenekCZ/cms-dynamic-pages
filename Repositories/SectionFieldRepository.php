<?php

namespace Modules\DynamicPages\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface SectionFieldRepository extends BaseRepository
{
}
