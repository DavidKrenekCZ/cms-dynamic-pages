<?php

namespace Modules\DynamicPages\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface CategoryGroupRepository extends BaseRepository
{
}
