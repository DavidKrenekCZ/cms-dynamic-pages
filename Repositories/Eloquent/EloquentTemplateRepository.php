<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\TemplateRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTemplateRepository extends EloquentBaseRepository implements TemplateRepository
{
}
