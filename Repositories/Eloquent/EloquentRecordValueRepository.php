<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\RecordValueRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRecordValueRepository extends EloquentBaseRepository implements RecordValueRepository
{
}
