<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\RecordRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRecordRepository extends EloquentBaseRepository implements RecordRepository
{
}
