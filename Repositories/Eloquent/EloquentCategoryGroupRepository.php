<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\CategoryGroupRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoryGroupRepository extends EloquentBaseRepository implements CategoryGroupRepository
{
}
