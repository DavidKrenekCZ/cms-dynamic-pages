<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\SectionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSectionRepository extends EloquentBaseRepository implements SectionRepository
{
}
