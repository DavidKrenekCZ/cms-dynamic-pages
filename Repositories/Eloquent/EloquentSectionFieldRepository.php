<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\SectionFieldRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSectionFieldRepository extends EloquentBaseRepository implements SectionFieldRepository
{
}
