<?php

namespace Modules\DynamicPages\Repositories\Eloquent;

use Modules\DynamicPages\Repositories\TemplateFieldRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTemplateFieldRepository extends EloquentBaseRepository implements TemplateFieldRepository
{
}
