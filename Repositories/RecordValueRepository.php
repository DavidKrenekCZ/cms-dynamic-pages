<?php

namespace Modules\DynamicPages\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface RecordValueRepository extends BaseRepository
{
}
