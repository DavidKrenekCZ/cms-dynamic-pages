<?php

namespace Modules\DynamicPages\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterDynamicPagesSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('dynamicpages::dynamicpages.title.dynamicpages'), function (Item $item) {
                $item->icon('fa fa-file');
                $item->weight(-1);
                $item->authorize(
                    $this->auth->hasAccess('dynamicpages.templates.index') ||
                    $this->auth->hasAccess('dynamicpages.records.index') ||
                    $this->auth->hasAccess('dynamicpages.categories.index')
                );
                $item->item(trans('dynamicpages::templates.title.templates'), function (Item $item) {
                    $item->icon('fa fa-paint-brush');
                    $item->weight(0);
                    $item->append('admin.dynamicpages.template.create');
                    $item->route('admin.dynamicpages.template.index');
                    $item->authorize(
                        $this->auth->hasAccess('dynamicpages.templates.index')
                    );
                });
                $item->item(trans('dynamicpages::records.title.records'), function (Item $item) {
                    $item->icon('fa fa-file-text-o');
                    $item->weight(0);
                    $item->append('admin.dynamicpages.record.create');
                    $item->route('admin.dynamicpages.record.index');
                    $item->authorize(
                        $this->auth->hasAccess('dynamicpages.records.index')
                    );
                });
                $item->item(trans('dynamicpages::categories.title.categories'), function (Item $item) {
                    $item->icon('fa fa-object-group');
                    $item->weight(0);
                    $item->append('admin.dynamicpages.category.create');
                    $item->route('admin.dynamicpages.category.index');
                    $item->authorize(
                        $this->auth->hasAccess('dynamicpages.categories.index')
                    );
                });
            });
        });

        return $menu;
    }
}
