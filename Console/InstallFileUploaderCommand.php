<?php

namespace Modules\DynamicPages\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Modules\DynamicPages\Http\Controllers\DynamicPagesController;

class InstallFileUploaderCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dynamicpages:install:fileuploader';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install a FileUploader library from repository.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $repo = $this->ask('Enter FileUploader repository link (format: https://{name}:{password}@{server}/{repo})');

        $folderPath = app_path("../Modules/Dynamicpages/Assets/fileuploader");
        
        // Repo already exists - pull
        if (file_exists($folderPath)) {
            $this->info("Repository already exists, pulling newest version...");
            chdir($folderPath);
            exec("git pull ".$repo, $output);

            \File::deleteDirectory(public_path("modules/dynamicpages/assets/fileuploader")); // remove public folder so it gets updated
        } else {    // Repo doesn't exist - clone
            $this->info("Repository doesn't exists, cloning...");
            exec("git clone ".$repo." ".$folderPath, $output);
        }

        foreach ($output as $item)
            $this->info($item);

        $version = trim($this->ask("What version do you want to checkout?", "HEAD"));
        if ($version != "" && $version != "HEAD") {
            chdir($folderPath);
            exec("git checkout ".$version);

            foreach ($output as $item)
                $this->info($item);
        }

        // Wait so that git is definitely loaded
        sleep(1);

        // Delete git folder (so that password is not stored anywhere)
        \File::deleteDirectory($folderPath."/.git");

        // Published new assets
        DynamicPagesController::publishAssets();

        $this->info("Done");
    }
}
