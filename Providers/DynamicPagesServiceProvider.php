<?php

namespace Modules\DynamicPages\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\DynamicPages\Events\Handlers\RegisterDynamicPagesSidebar;
use Modules\DynamicPages\Console\InstallFileUploaderCommand;

class DynamicPagesServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterDynamicPagesSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('dynamicpages', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\DynamicPages\Repositories\TemplateRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentTemplateRepository(new \Modules\DynamicPages\Entities\Template());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheTemplateDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\TemplateFieldRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentTemplateFieldRepository(new \Modules\DynamicPages\Entities\TemplateField());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheTemplateFieldDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\SectionRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentSectionRepository(new \Modules\DynamicPages\Entities\Section());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheSectionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\SectionFieldRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentSectionFieldRepository(new \Modules\DynamicPages\Entities\SectionField());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheSectionFieldDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\RecordRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentRecordRepository(new \Modules\DynamicPages\Entities\Record());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheRecordDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\RecordValueRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentRecordValueRepository(new \Modules\DynamicPages\Entities\RecordValue());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheRecordValueDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\CategoryGroupRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentCategoryGroupRepository(new \Modules\DynamicPages\Entities\CategoryGroup());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheCategoryGroupDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DynamicPages\Repositories\CategoryRepository',
            function () {
                $repository = new \Modules\DynamicPages\Repositories\Eloquent\EloquentCategoryRepository(new \Modules\DynamicPages\Entities\Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DynamicPages\Repositories\Cache\CacheCategoryDecorator($repository);
            }
        );
// add bindings








    }

    /**
     * Register the console commands
     */
    private function registerCommands()
    {
        $this->commands([
            InstallFileUploaderCommand::class
        ]);
    }

}
